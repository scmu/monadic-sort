Dear Shin-Cheng Mu,

On behalf of the FLOPS'2020 Program Committee, we are delighted to
inform you that your submission titled

   Declarative Pearl: Deriving Monadic Quicksort

has been accepted for publication and presentation at the symposium.

Each submission has gone through a rigorous review process, receiving
four and in some cases five reviews. This was followed by an online
discussion among the program committee members, where the pros
and cons of each submission were discussed, sometimes extensively.
Below you can find your reviews.  We hope that you will find the
comments and suggestions in them useful in revising and improving
your paper.

You will also receive further instructions about how you should submit
the final version of your paper.  Note that the final version deadline
mentioned in the FLOPS site (20 Feb) is pretty tight.

Thank you for submitting to FLOPS'2020 and looking forward to seeing you
in Akita in late April!

Sincerely,

Keisuke Nakano and Kostis Sagonas (PC Chairs)

SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort


----------------------- REVIEW 1 ---------------------
SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort
AUTHORS: Shin-Cheng Mu and Tsung-Ju Chiang

----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----- TEXT:
This pearl shows how monadic abstractions and laws can be used for
 program derivation, by deriving two versions of quicksort: a
 list-based one that is pure and an array-based one with in-place
 swapping.

 It is very well written and clearly accomplishes what it
 sets out to do. It is also easy to follow, taking the reader by the
 hand and guiding them through the two interesting derivations. The
 second part, the array-based sort, is slightly dense in its
 presentation---but I don't have a clear concrete suggestion for
 improvement.

 The technical contribution of this work is fairly small---I don't
 see anything really novel---but as a declarative pearl, it was an
 interesting read that others might also enjoy.

 Minor:

 p7: Missing space between "write i x" and "and"

 p8: The tail-recursive version of partl presented will cause a space
 leak in Haskell (as the accumulators will be represented as thunks
 that increase in size as the input list is traversed). It's not
 really important (the technique described in the paper is not really
 Haskell-specific despite relying pretty heavily on Haskell notation
 and abstractions), but using the tail recursive version over the
 regular one in a lazy language usually requires more thought.



----------------------- REVIEW 2 ---------------------
SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort
AUTHORS: Shin-Cheng Mu and Tsung-Ju Chiang

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
The paper illustrates how list- and array-based quicksort functions can be
derived from slow sort functions.  There are several papers that aim to
verify the quicksort algorithm and also to derive quicksort algorithms in
the context of program optimization.  However, the purpose of the paper is
a bit different from them; it is actually to demonstrate that monads
provide a good framework for program refinement/calculus.  The author uses
monads to represent non-deterministic computation.

In the first half of the paper the author sets up a monad-based framework,
and then illustrates refinement processes in the framework by deriving a
list-based quicksort function from a non-deterministic specification of the
slowsort function.  The second half is denoted to derive an in-place
version of quicksort.  In contrast to the list-based one, this is highly
non-trivial but the author

The presented framework is interesting and its usefulness is convincingly
illustrated.  The quality of the presentation is also very good.  The paper
would be readable even if the reader is unfamiliar with monadic programming
and/or program refinement.  Thus, I am in favor of accepting the paper.


SUGGESTIONS

Termination:  Why not discuss termination of the resulting programs?
Equational proofs only guarantee partial correctness of the program.
For total correctness one also needs to prove termination of (recursive)
functions.  Formal proofs of termination would go beyond the scope of this
paper, but including a short remark on termination would be appreciated.

Complexity:  Similarly, a short remark on the time complexities of the
resulting functions would be appreciated, as achieving the quadratic time
complexity is one of main purposes of the program refinements.


MINOR COMMENTS

page 2, $-notation: The notation is unused in the rest of paper, I think.

page 3, middle: this pearl use"s"

page 5, middle: It might be helpful to remark that f = g is equivalent to
the inclusions f \sqsubseteq g and g \sqsubseteq f.

page 8: in a"n" expression

page 13: I would use Kleisli composition >=> only in this section, as
the main part does not really require the notation.  In fact Kleisli
compositions are used only three times:
- p.4:  "slowsort  =  perm >=> filt sorted"
- p.12: "perm >=> perm  =  perm"
- p.12: "perm >=> slowsort  =  slowsort"



----------------------- REVIEW 3 ---------------------
SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort
AUTHORS: Shin-Cheng Mu and Tsung-Ju Chiang

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
The paper shows how to derive two variations of quicksort - one pure on
lists and one imperative on arrays - from a specification. The idea is to
be able to take a specification which matches a problem description but
is potentially inefficient, then transform it to an efficient program with
a series of steps justified by some mathematical reasoning. Sorting is an
interesting choice because, although it is well studied, there's still a lot
of choices which can be made in the implementation, not only the choice
of algorithm, but what happens with equal values for example. (In fact, it
might be good choice *because* it is well studied!) Thus, there might be
several valid implementations of a specification producing different
answers.

I quite like the idea here, and it's nice to see two different derivations
from the same specification. Also, it's nice to see the array based version
which really is quick sort in that it works in-place, rather than the
pure version which is really tree sort in practice. For me, the paper fits the
definition of a pearl, reads nicely, and proceeds at a good pace, so I
recommend it be accepted.  Some more specific comments follow.

p1 Even if the specification can't resolve the tie, surely the output would
always be the same? I suppose it depends how equality is defined.
p2 "It was complained that the notations are too bizarre" - by whom?
p2 You remark on this later, but I was surprised to see this definition of
Monad given the current Haskell standard (so "usually modelled" might not
be the right way of putting it)
p3 does the name "Elm" suggest "Element"? And, given that you're writing
Haskell here, what do we need to know about it to write the specification,
and how do you express that in code?
p4 Earlier, you say that the specification "obviously matches" the problem
description. Even for sorting, it's a bit involved, and 'perm' might itself
have errors, so is there anything you can do about this? (You can only
prove so much, of course, but it'd be good to note the limitations)
p5 "Proof of Lemma 1" => "The proof of Lemma 1" (there's missing articles in
a few other places, e.g. often "non-determinism monad" rather than "the/a
non-determinism monad".
p5 A small thing, but since the code is essentially Haskell, I'd prefer to
see 'return []' than '{[]}'.
p6 "it is not hard to show that..." - if so can you give a sentence or two that
explains this?
p8 The point of "MonadPlus" in some cases is to be clear where the
non-determinism is needed, yes? Is there a concrete monad that satisfies
these constraints? Perhaps give a concrete example at the end of the
section.
p9 "it is costly to mutate..." - because of the insertion of 'us' or the
appending 'vs'? A bit more explanation of why would help here.
p9 "it turns out to be easier if..." - this suggests some insight is
required during the derivation (as I'd expect) that you haven't explained
here. How many things did you try that didn't work so nicely?
p14 "demonstrate that monad is a good choice" - which monad? I think you
just mean the monadic style is a good choice?



----------------------- REVIEW 4 ---------------------
SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort
AUTHORS: Shin-Cheng Mu and Tsung-Ju Chiang

----------- Overall evaluation -----------
SCORE: 2 (accept)
----- TEXT:
Summary:

This paper presents derivation of two versions of quicksort, one for
sorting lists and the other for sorting arrays. An important feature
of the derivation is its "functional" style, starting from a
functional specification of a slow sorting using the non-determinism
monad, and deriving a functional implementation that sorts a list, and
an imperative implementation (in a function over a stateful array
monad) that sorts an array. The basic idea of this derivation is to
use "refinement" for manipulation of functions based on monads. This
is different from the traditional approach based on relations.

Favorite Points:

+ I enjoyed reading this paper. Although derivation of sorting
 algorithms have been studied for a long time, the idea of functional
 derivation based on monads and refinement sounds new and
 interesting.

+ The derivation of a sorting algorithm on arrays is indeed
 nontrivial, which gives a good demonstration of the power of the
 approach.

+ The paper is clearly presented.

Unclear Points:

- There is much room for improving derivation of quicksort on arrays.
 For instance, (1) The definition of the array monad should be given;
 and (2) The relationship between "read i" and "readList i x" and
 that between "write i x" and "writeList i xs" are missing.

- While I can understand the intuitive meaning of "length preserving",
 I cannot see clearly why perm is length preserving, satisfying
 the following equation

    perm xs >>= \ys -> {(ys; #ys)} = perm xs >>= \ys -> {(ys;#xs)}

 according to the definition of "length preserving".

- This paper does not contain discussion on related work. It would be
 better if a good comparison with other related approaches is
 given. Also, I'd like to see discussion on its relation with program
 reasoning based on algebraic effects, which has been intensively
 studied recently.



----------------------- REVIEW 5 ---------------------
SUBMISSION: 8
TITLE: Declarative Pearl: Deriving Monadic Quicksort
AUTHORS: Shin-Cheng Mu and Tsung-Ju Chiang

----------- Overall evaluation -----------
SCORE: 1 (weak accept)
----- TEXT:
This paper presents itself as a pearl that demonstrates how refinement of
functional programs from specificaitions can be achieved by reasoning about
monadic computations. The paper demonstrates the methodology by working through
two refinements of quicksort: one on lists, and the other on arrays.

On the whole I think that this is a worthwile paper and the calculations are
interesting: although they are certainly not straight-forward. The writing is
carefully constructed and does a relatively good job of motivating much of the
material. There are a few places where properties are stated without
justification or proper grounding, but I think that this is not easily avoided.

As a pearl I do not expect novel results, but I do expect appropriate
contextualisation of the work and a discussion of related ideas. I think that
this is critical to the publishing of this work.

I am certain that the author is aware of the "Just do it" paper by Gibbons and
Hinze, and so I am somewhat surprised that it is not even mentioned, given the
commonality in goals and similar techniques employed. Even if the author
believes that the goals of that paper are slightly different, there ought to be
some discussion.

On a related note my biggest critique concerns some of the notation that is
used. I must admit that I am somewhat sceptical about the reversing of the
usual refinement operation: I am quite unhappy about the introduction of a
symbol that has the exact opposite meaning of the one already in the
literature, and without what seems to be proper motivation.  The choice comes
down to the equation at the top of p5, and here I would expect that m2 is less
refined than m1, since it is has more nondeterminism. Usually the specification
offers choice, and this is cut down by a refined version. This is in line with
the standard calculus, where the least refined program is bottom.

The fact is that there is a huge literature on the relationship between
nondeterminism and program refinement which seems to have been largely ignored
here. (The Egli-Milner ordering on sets is relevant, as is work by Plotkin
dating back to 1975. The author may wish to consult "Algebraic Approaches to
Nondeterminism: An Overview." by Walicki and Meldal which outlines some of this
and has plenty of pointers to the literature). Perhaps I have missed some
detail that justifies the choice of the author, but there ought to be more
discussion if this is the case.


Minor remarks
-------------

The paper is generally very well written, and I only have a few minor typos to
point out:

p1 "albeit inefficient" ~>
  "albeit inefficiently"

p1 "It would be inflexible, and if not impossible, to decide in the
  specification how to resolve the tie":
  This is certainly not impossible. It
  is quite standard to talk about the stability of a sorting algorithm, and this
  can be specified formally. I also don't understand why it would be
  inflexible.

p2 "There has been attempts" ~>
  "There have been attempts"

p2 "Problem specifications are given as non-deterministic monads":
  No, they are given as Kleisli arrows for non-deterministic monads. The
  arrows are not the monads in the same way that functions are not their
  types.

p5 The definition of \subseteq could be reversed to avoid the problem of
  refinement being the wrong way around. Then we would establish a refinement
  from a nondeterministic program to a more deterministic one, as is standard.

p6 Here I wonder if the calculation would be better performed using
  do-notation, which I'm sure the author is aware of. Perhaps they are
  avoiding this so that we are not working in Haskell?

p13 "From a specification of sorting using non-determinism monad" ~>
   "From a specification of sorting using the non-determinism monad"
