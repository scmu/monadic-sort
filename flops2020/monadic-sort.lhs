% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.20 of 2017/10/04

\documentclass[runningheads]{llncs}

% build using
%    lhs2TeX monadic-sort.lhs | pdflatex --jobname=monadic-sort
% build bibliography by
%    bibtex -min-crossrefs=20 monadic-sort

%include lhs2TeX.fmt
%include forall.fmt
%include polycode.fmt
%include common/Formatting.fmt

%include common/Monad.fmt
%include common/Paper.fmt

\usepackage{amsmath}
\usepackage{mathpartir}
\usepackage{amsfonts}
\usepackage{stmaryrd}
%\usepackage{mathptmx}
%\usepackage{amsthm}
\usepackage{hyperref}
% If you use the hyperref package, please uncomment the following line
% to display URLs in blue roman font according to Springer's eBook style:
\renewcommand\UrlFont{\color{blue}\rmfamily}
\usepackage{scalerel}
\usepackage{bussproofs}
\EnableBpAbbreviations
\usepackage{url}
\usepackage{subfig}
\usepackage{enumitem}
\usepackage{mdframed}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{bm}

\usepackage{common/doubleequals}

%\setlength{\mathindent}{15pt}

\newcommand{\todo}[1]{{\bf [To do: #1]}}
\newcommand{\delete}[1]{}

\allowdisplaybreaks

\newcommand{\scm}[1]{\textcolor{teal}{#1}}}

\begin{document}

\title{Declarative Pearl:
Deriving Monadic Quicksort%
%\thanks{Supported by organization x.}
}
\author{Shin-Cheng Mu\inst{1} \and
Tsung-Ju Chiang\inst{2}}
%\author{~}
%
%\authorrunning{Mu and Chiang}
% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Academia Sinica, Taiwan \and
 National Taiwan University, Taiwan}
%\institute{~}

%
\maketitle              % typeset the header of the contribution
%
\begin{abstract}
To demonstrate derivation of monadic programs,
we present a specification of sorting using the non-determinism monad, and derive pure quicksort on lists and state-monadic quicksort on arrays.
In the derivation one may switch between point-free and pointwise styles, and deploy techniques familiar to functional programmers such as pattern matching and induction on structures or on sizes.
% The pre- and post-conditions of a stateful program can be expressed in terms of factors, and
Derivation of stateful programs resembles reasoning backwards from the postcondition.
% Some problems are better modelled as non-deterministic mappings from the input to all outputs that are equally preferred, which is then refined to a deterministic computation through derivation, during which we are given flexibility to choose which output to return.
% %In the 90's, a calculus of relations was developed to perform such derivation, which unfortunately did not gain acceptance it deserves in the functional programming community, perhaps due to its complexity and the need to reason in point-free style.
% We propose a monadic calculus for such derivations.
% It captures key concepts of the relational approach developed in the 90's, including the refinement order and factors, while allowing the users to mix point-free and pointwise styles and manipulate programs in ways similar to functional program reasoning.
% In addition, in this monadic approach we can talk about other effects such as state.
% As examples, we present derivations of a program computing a minimum element of a list, the pure quicksort for lists, and the imperative quicksort for arrays.
\keywords{monads \and program derivation \and equational reasoning \and nondeterminism \and state \and quicksort}
\end{abstract}

%include sections/Intro.lhs
%include sections/Monads.lhs
%include sections/Spec.lhs
%include sections/QSort.lhs
%include sections/IQSort.lhs
%include sections/Conclusions.lhs

\bibliographystyle{splncs04}
\bibliography{monadic-sort.bib}
%\input{monadic-sort.bbl}


\end{document}
