%if False
\begin{code}
{-# LANGUAGE TypeOperators, FlexibleContexts #-}
module Monads where

import Prelude hiding (any, read, readList)
import Control.Monad
import Data.Array

import Common

\end{code}
%endif

\section{Monads}
\label{sec:monads}

A monad consists of a type constructor |m :: * -> *| paired with two operators, can be modelled in Haskell as a type class:
\begin{spec}
class Monad m where
    return  :: a ->  m a
    (>>=)   :: m  a -> (a -> m b) -> m b {-"~~."-}
\end{spec}
The operator |return| is usually called $\Varid{return}$ or $\Varid{unit}$.
Since it is used pervasively in this pearl, we use a shorter notation for brevity.
% (and write its prefix form as |return|) .
One can either think of it as mimicking the notation for a singleton
set, or C-style syntax for a block of effectful program.
They should satisfy the following \emph{monad laws}:
\begin{align*}
  |m >>= return| &= |m| \mbox{~~,}\\
  |return x >>= f| &= |f x| \mbox{~~,} \\
  |(m >>= f) >>= g| &= |m >>= (\x -> f x >>= g)| \mbox{~~.}
\end{align*}

A standard operator |(>>) :: Monad m => m a -> m b -> m b|, defined by |m1 >> m2 = m1 >>= \ _ -> m2|, is handy when we do not need the result of |m1|.
Monadic functions can be combined by Kleisli composition |(>=>)|,
defined by |f >=> g {-"~"-}  = {-"~"-} \x -> f x >>= g|.
%It is also convenient to have an |(<$>)| operator that applies a pure function to a monadic argument: |f <$> m {-"~"-}  = {-"~"-} m >>= \x -> return (f x)|.
% Both operators are defined below:
% \begin{spec}
%   (>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
%   f >=> g {-"~"-}  = {-"~"-} \x -> f x >>= g {-"~~,"-}
%
%   (<$>) :: Monad m => (a -> b) -> m a -> m b
%   f <$> m {-"~"-}  = {-"~"-} m >>= \x -> return (f x) {-"~"-}  = {-"~"-} m >>= (return . f) {-"~~."-}
% \end{spec}

Monads usually come with additional operators corresponding to the effects they provide.
% For this paper we are concerned with only two effects --- non-determinism and state.
% The latter will be introduced in Section~\ref{sec:iqsort}.
Regarding non-determinism, we assume two operators |mzero| and |mplus|, respectively denoting failure and non-deterministic choice:
\begin{spec}
class Monad m => MonadPlus m where
  mzero  :: m a
  mplus  :: m a -> m a -> m a {-"~~."-}
\end{spec}
It might be a good time to note that this pearl uses type classes for two purposes: firstly, to be explicit about the effects a program uses.
%For example, programs using non-determinism are labelled with constraint |MonadPlus m|.
Secondly, the notation implies that it does not matter which actual implementation we use for |m|, as long as it satisfies all the properties we demand ---
as Gibbons and Hinze \cite{GibbonsHinze:11:Just} proposed, we use the properties, not the implementations, when reasoning about programs.
The style of reasoning in this pearl is not tied to type classes or Haskell,
and we do not strictly follow the particularities of type classes in the current Haskell standard.%
\footnote{For example, we overlook that a |Monad| must also be |Applicative|, |MonadPlus| be |Alternative|, and that functional dependency is needed in a number of places.}

It is usually assumed that |mplus| is associative with |mzero| as its identity:
\begin{align*}
  |mzero `mplus` m| & = |m| ~=~ |m `mplus` mzero| \mbox{~~,} &
     %\label{eq:mzero-id}
  |(m1 `mplus` m2) `mplus` m3| &=
     |m1 `mplus` (m2 `mplus` m3)| \mbox{~~.}
     %\label{eq:mplus-assoc}
\end{align*}
For the purpose of this pearl, we also demand that |mplus| be idempotent and commutative. That is, |m `mplus` m = m| and |m `mplus` n = n `mplus` m|.
% \begin{align*}
%   |m `mplus` m| & = |m|  \mbox{~~,}\\
%   |m `mplus` n| & = |n `mplus` m| \mbox{~~.}
% \end{align*}
%In this aspect, our non-determinism monad is like a \emph{set monad}.
Efficient implementations of such monads have been proposed (e.g. \cite{Kiselyov:13:How}).
However, we use non-determinism monad only in specification.
The derived programs are always deterministic.

The laws below concern interaction between non-determinism and |(>>=)|:
\begin{align}
  |mzero >>= f| & = |mzero| \label{eq:nd-left-zero}\mbox{~~,}\\
  |m >> mzero| & = |mzero| \label{eq:nd-right-zero}\mbox{~~,}\\
  |(m1 `mplus` m2) >>= f| &= |(m1 >>= f) `mplus` (m2 >>= f)| \mbox{~~,}
     \label{eq:nd-left-distr}\\
  |m >>= (\x -> f1 x `mplus` f2 x)| &= |(m >>= f1) `mplus` (m >>= f2)| \mbox{~~.}
     \label{eq:nd-right-distr}
\end{align}
Left-zero~\eqref{eq:nd-left-zero} and left-distributivity~\eqref{eq:nd-left-distr} are standard --- the latter says that |mplus| is algebraic.
When mixed with state, right-zero \eqref{eq:nd-right-zero} and right-distributivity \eqref{eq:nd-right-distr} imply that each non-deterministic branch has its own copy of the state~\cite{Pauwels:19:Handling}.
