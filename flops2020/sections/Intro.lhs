\section{Introduction}
\label{sec:intro}

% This pearl presents a specification of sorting given in terms of non-determinism monad, from which we derive quicksort on arrays, implemented using state monad.
% The purpose is to demonstrate reasoning and derivation of monadic programs.

This pearl presents two derivations of quicksort.
The purpose is to demonstrate reasoning and derivation of monadic programs.
In the first derivation we present a specification of sorting using the non-determinism monad, from which we derive a pure function that sorts a list.
In the second derivation we derive an imperative algorithm, expressed in terms of the state monad, that sorts an array.

Before we dive into the derivations, we shall explain our motivation.
Program derivation is the technique of formally constructing a program from a problem specification.
In functional derivation, the specification is a function that obviously matches the problem description, albeit inefficiently.
It is then stepwise transformed to a program that is efficient enough,
where every step is justified by mathematical properties guaranteeing that the program {\em equals} the specification, that is, for all inputs they compute exactly the same output.

It often happens, for certain problem, that several answers are equally preferred.
In sorting, for example, the array to be sorted might contain items with identical keys.
It would be inflexible, if not impossible, to decide in the specification how to resolve the tie: it is hard to predict how quicksort arranges items with identical keys before actually deriving quicksort.%
\footnote{Unless we confine ourselves to stable sorting.}
Such problems are better modelled as non-deterministic mappings from the input to all valid outputs. The derived program no longer equals but {\em refines} the specification.%
\footnote{This is standard in imperative program derivation ---
Dijkstra~\cite{Dijkstra:76:Discipline}
% %, when designing Guarded Command Language,
argued that we should take non-determinism as default and determinism as a special case.}

To cope with non-determinism, there was a trend in the 90's generalising from functions to relations~\cite{BackhousedeBruin:91:Relational,% Backhouse:91:Polynomial,%
BirddeMoor:97:Algebra}.
Although these relational calculi are, for advocates including the authors of this paper, concise and elegant, for those who were not following this line of development, these calculi are hard to comprehend and use.
People, in their first and often only exposure to the calculi, often complained that the notations are too bizarre, and reasoning with inequality (refinement) too complex.
One source of difficulties is that notations of relational calculus are usually \emph{point-free} --- that is, about composing relations instead of applying relations to arguments.
%
There have been attempts (e.g \cite{deMoorGibbons:00:Pointwise,BirdRabe:19:How}) designing {\em pointwise} notations, which functional programmers are more familiar with.
Proposals along this line tend to exhibit confusion
%when non-deterministic values are passed to functions
when functions are applied to non-deterministic values
 --- $\beta$-reduction and $\eta$-conversion do not hold.
One example~\cite{deMoorGibbons:00:Pointwise} is that
|(\x -> x - x) (0 `mplus` 1)|, where |mplus| denotes non-deterministic choice, always yields |0|, while |(0 `mplus` 1) - (0 `mplus` 1)| could be |0|, |1|, or |-1|.

Preceding the development of relations for program derivation, another way to model non-determinism has gained popularity.
Monads~\cite{Moggi:89:Computational} were introduced into functional programming as a way to rigorously talk about side effects including IO, state, exception, and non-determinism.
Although they are considered one of the main obstacles in learning functional programming (in particular Haskell), monads have gained wide acceptance.
In this pearl we propose a calculus of program derivation based on monads --- essentially moving to a Kleisli category.
Problem specifications are given as Kleisli arrows for non-deterministic monads, to be refined to deterministic functional programs through calculation.
One of the benefits is that functional programmers may deploy techniques they are familiar with when reasoning about and deriving programs.
These include both point-free and pointwise reasoning, and induction on structures or sizes of data types.
An additional benefit of using monads is that we may talk about effects other than non-determinism. We demonstrate how to, from a specification of quicksort on lists, construct the imperative quicksort for arrays. All the derivations and theorems in this pearl are verified in the dependently typed programming language Agda.
