\section{Conclusions}
\label{sec:conclusions}

From a specification of sorting using the non-determinism monad, we have derived a pure quicksort for lists and a state-monadic quicksort for arrays.
We hope to demonstrate that the monadic style is a good choice as a calculus for program derivation that involves non-determinism.
One may perform the derivation in pointwise style, and deploy techniques that functional programmers have familiarised themselves with, such as pattern matching and induction on structures or on sizes.
When preferred, one can also work in point-free style with |(>=>)|.
Programs having other effects can be naturally incorporated into this framework.
% The pre- and post-conditions of a stateful program can be expressed in terms of factors, and
The way we derive stateful programs echos how we, in Dijkstra's style, reason backwards from the postcondition.

A final note:
|(>=>)| and |sqse| naturally induce the notion of (left) factor, |(\\) :: (a -> m b) -> (a -> m c) -> b -> m c|, defined by the Galois connection:
\begin{align*}
  |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}| &  |<=>{-"~"-} g {-"\,"-}`sqse`{-"\,"-} f \\ h {-"~~."-}| \label{eq:left-factor-galois}
\end{align*}
%if False
\begin{code}
(\\) :: Monad m => (a -> m b) -> (a -> m c) -> (b -> m c)
(\\) = undefined
\end{code}
%endif
% \begin{spec}
% (\\)  :: Monad m => (a -> m b) -> (a -> m c) -> b -> m c {-"~~,"-}
% (/)   :: Monad m => (a -> m c) -> (b -> m c) -> a -> m b {-"~~."-}
% \end{spec}
Let |h :: a -> m c| be a monadic specification, and |f :: a -> m b| performs the computation half way,
then |f \\ h| is the most non-deterministic (least constrained) monadic program that, when ran after the postcondition set up by |f|, still meets the result specified by |h|. With |(\\)|, |ipartl| and |iqsort| can be specified by:
\begin{spec}
   ipartl p i  `sqse` write3L i \\ ((second perm . partl p) >=> write2L i) {-"~~,"-}
   iqsort i    `sqse` writeL i \\ (slowsort >=> writeList i) {-"~~."-}
\end{spec}
In relational calculus, the {\em factor} is an important operator that is often associated with weakest precondition.
We unfortunately cannot cover it due to space constraints.


% Note that the specification above can also be written in terms of factor:
% %if False
% \begin{code}
% ipartlSpec2 :: (MonadArr Elm m, MonadPlus m) =>
%   Elm -> Int -> (Int, Int, Int) -> m (Int, Int)
% ipartlSpec2 p i =
% \end{code}
% %endif
% \begin{code}
%   ipartl p i `sqse` write3L i \\ ((second perm . partl p) >=> write2L i) {-"~~."-}
% \end{code}
% In words, we want |ipartl p i| to be a refinement of the largest program which, when run in a state initialised by |write3L i|, always produces a result specified by |(second perm . partl p) >=> write2L i|.
