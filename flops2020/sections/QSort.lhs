%if False
\begin{code}
{-# LANGUAGE TypeOperators #-}
module QSort where

import Prelude hiding (any)
import GHC.Base (Alternative(..))
import Control.Monad

import Common
import Monads
import Spec
\end{code}
%endif

\section{Quicksort on Lists}
\label{sec:quicksort}

In this section we derive a divide-and-conquer property of |slowsort|.
It allows us to refine |slowsort| to the well-known recursive definition of quicksort on lists, and is also used in the next section to construct quicksort on arrays.

\subsubsection*{Refinement}
We will need to first define our concept of program refinement.
We abuse notations from set theory and define:
\begin{spec}
  m1 `sse` m2  {-"~"-}<=>{-"~"-} m1 `mplus` m2 = m2 {-"~~."-}
\end{spec}
The righthand side |m1 `mplus` m2 = m2| says that every result of |m1| is a possible result of |m2|.
When |m1 `sse` m2|, we say that |m1| \emph{refines} |m1|, |m2| can be \emph{refined to} |m1|, or that |m2| \emph{subsumes} |m1|.
Note that this definition applies not only to the non-determinism monad, but to monads having other effects as well.
We denote |sse| lifted to functions by |sqse|:%
\begin{spec}
  f `sqse` g {-"~"-}={-"~"-} (forall x : f x `sse` g x) {-"~~."-}
\end{spec}
That is, |f| refines |g| if |f x| refines |g x| for all |x|.%
% \footnote{Unfortunately, in refinement calculus for imperative programs, |p `sqse` q| denotes that |q| refines |p|.
% In this pearl we let |sqse| be consistent with the direction of |sse|.}
When we use this notation, |f| and |g| are always functions returning monads, which is sufficient for this pearl.

One can show that the definition of |sse| is equivalent to
|m1 `sse` m2  {-"\,"-}<=>{-"\,"-} (exists n : m1 `mplus` n = m2)|,
% \begin{spec}
%   m1 `sse` m2  {-"~"-}<=>{-"~"-} (exists n : m1 `mplus` n = m2) {-"~~,"-}
% \end{spec}
and that |sse| and |sqse| are both reflexive, transitive, and anti-symmetric (|m `sse` n && n `sse` m {-"\,"-}<=>{-"\,"-} n = m|). %and so is |sqse|.
Furthermore, |(>>=)| respects refinement:
\begin{lemma}\label{lma:refine-bind-preservation}
Bind |(>>=)| is monotonic with respect to |sse|.
That is,
|m1 `sse` m2 {-"\,"-}==>{-"\,"-} m1 >>= f `sse` m2 >>= f|, and
|f1 `sqse` f2 {-"\,"-}==>{-"\,"-} m >>= f1 `sse` m >>= f2|.
% \begin{enumerate}
% \item |m1 `sse` m2 {-"~"-}==>{-"~"-} m1 >>= f `sse` m2 >>= f|, and
% \item |f1 `sqse` f2 {-"~"-}==>{-"~"-} m >>= f1 `sse` m >>= f2|.
% \end{enumerate}
\end{lemma}
Having Lemma~\ref{lma:refine-bind-preservation} allows us to refine programs in a compositional manner.
The proof of Lemma~\ref{lma:refine-bind-preservation} makes use of \eqref{eq:nd-left-distr} and \eqref{eq:nd-right-distr}.

\subsubsection*{Commutativity and |guard|}
We say that |m| and |n| commute if
\begin{spec}
m >>= \x -> n >>= \y -> f x y {-"~"-}={-"~"-} n >>= \y -> m >>= \x -> f x y {-"~~."-}
\end{spec}
% If |m| and |n| commute for all |m| using effect |X| and |n| using effect |Y|, we say that effects |X| and |Y| commute.
It can be proved that |guard p| commutes with all |m| if non-determinism is the only effect in |m| --- a property we will need many times.
Furthermore, having right-zero \eqref{eq:nd-right-zero} and right-distributivity \eqref{eq:nd-right-distr}, in addition to other laws, one can prove that non-determinism commutes with other effects.
In particular, non-determinism commutes with state.

We mention two more properties about |guard|:
|guard (p&&q)| can be split into two, and |guard|s with complementary predicates can be refined to |if|:
% \begin{equation}
% |guard (p && q)| ~=~ |guard p >> guard q| \mbox{~~.}
% \label{eq:guard-conj-split}
% \end{equation}
% secondly, a pair of |guard|s with complementary predicates can be refined to |if|:
% \begin{spec}
% (guard p >> m1) `mplus` (guard (not . p) >> m2) {-"~"-}`spe`{-"~"-} if p then m1 else m2 {-"~~."-}
% \end{spec}
\begin{align}
|guard (p && q)| &~= |guard p >> guard q| \mbox{~~,}
\label{eq:guard-conj-split}\\
|(guard p >> m1)| &|`mplus` (guard (not . p) >> m2)| ~|`spe`{-"~"-} if p then m1 else m2| \mbox{~~.}
\label{eq:guard-if}
\end{align}

\subsubsection*{Divide-and-Conquer}
Back to |slowsort|. We proceed with usual routine in functional programming: case-analysis on the input.
For the base case, |slowsort [] = return []|.
For the inductive case, the crucial step is the commutativity of |guard|:
%if False
\begin{code}
slowsort_ind_der :: MonadPlus m => Elm -> List Elm -> m (List Elm)
slowsort_ind_der p xs =
\end{code}
%endif
\begin{code}
      slowsort (p:xs)
 ===    {- expanding definitions, monad laws -}
      split xs >>= \(ys, zs) ->
      perm ys >>= \ys' -> perm zs >>= \zs' ->
      filt sorted (ys' ++ [p] ++ zs')
 ===    {- by \eqref{eq:sorted-cat3} -}
      split xs >>= \(ys, zs) ->
      perm ys >>= \ys' -> perm zs >>= \zs' ->
      guard (sorted ys' && sorted zs' && all (<=p) ys' && all (p<=) zs') >>
      return (ys' ++ [p] ++ zs')
 ===    {- \eqref{eq:guard-conj-split} and that |guard| commutes with non-determinism -}
      split xs >>= \(ys, zs) -> guard (all (<=p) ys && all (p<=) zs') >>
      (perm ys >>= filt sorted) >>= \ys' ->
      (perm zs >>= filt sorted) >>= \zs' ->
      return (ys' ++ [p] ++ zs') {-"~~."-}
\end{code}
Provided that we can construct a function |partition| such that
%if False
\begin{code}
partition_spec :: (Ord a, MonadPlus m) => a -> [a] -> m ([a], [a])
partition_spec p xs =
\end{code}
%endif
\begin{code}
 return (partition p xs) {-"~"-}`sse`{-"~"-} split xs >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs) {-"~~,"-}
\end{code}
we have established the following divide-and-conquer property:
\begin{equation}
\begin{split}
|slowsort (p:xs)| ~~\supseteq~~ & |return (partition p xs) >>= \(ys, zs) ->|\\[-1mm]
                       & |slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->|\\[-1mm]
                       & |return (ys' ++ [p] ++ zs') {-"~~."-}|
\end{split}
\label{eq:slowsort-rec}
\end{equation}

% A derivation works by constructing |qsort| and, at the same time, building a proof that |qsort| satisfies the specification.
% In this case, the proof works by induction on the size of its input.
% Therefore, we assume that the specification |return (qsort ws) `sse` perm ws >>= filt sorted| holds for |ws| whose length is shorter than |p:xs|. In particular, it holds for |ys| and |zs|. We therefore have:
% \begin{spec}
%       slowsort (p:xs)
%  `spe` {- \eqref{eq:slowsort-rec} and inductive assumption -}
%       return (partition p xs) >>
%       return (qsort ys) >>= \ys' -> return (qsort zs) >>= \zs' ->
%       return (ys' ++ [p] ++ zs')
%  ===   {- monad laws -}
%       return (let (ys,zs) = partition p xs in qsort ys ++ [p] ++ qsort zs) {-"~~."-}
% \end{spec}
% In summary, we have derived
% \begin{code}
% qsort []      =  []
% qsort (p:xs)  =  let (ys,zs) = partition p xs
%                  in qsort ys ++ [p] ++ qsort zs {-"~~."-}
% \end{code}

The derivation of |partition| proceeds by induction on the input.
In the case for |xs := x:xs| we need to refine two guarded choices, |(guard (x<=p) >> return (x:ys, zs)) `mplus` (guard (p<=x) >> return (ys, x:zs))|, to an |if| branching. When |x| and |p| equal, the specification allows us to place |x| in either partition. For no particular reason, we choose the left partition. That gives us:
\begin{code}
partition p []      =  ([],[])
partition p (x:xs)  =  let (ys,zs) = partition p xs
                       in if x <= p then (x:ys, zs) else (ys, x:zs) {-"~~."-}
\end{code}
Having |partition| derived, it takes only a routine induction on the length of input lists to show that |return . qsort `sqse` slowsort|, where |qsort| is given by:
\begin{code}
qsort []      =  []
qsort (p:xs)  =  let (ys,zs) = partition p xs
                 in qsort ys ++ [p] ++ qsort zs {-"~~."-}
\end{code}
As is typical in program derivation, the termination of derived program is shown separately afterwards. In this case, |qsort| terminates because the input list decreases in size in every recursive call --- for that we need to show that, in the call to |partition|, the sum of lengths of |ys| and |zs| equals that of |xs|.

% The remaining task is to construct |partition p xs|. The case for |xs := []| is again immediate.
% Consider |x:xs|, assuming that the specification holds for |xs|:
% %if False
% \begin{code}
% partition_ind_der ::
%   (MonadPlus m, Ord a) => a -> a -> [a] -> m ([a], [a])
% partition_ind_der p x xs =
% \end{code}
% %endif
% \begin{code}
%    split (x:xs) >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
%  ===       {- definition of |split| -}
%    (split xs >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)) >>=
%       filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
%  ===       {- monad laws, left-distributivity, definition of |filt| -}
%    split xs >>= \(ys, zs) ->
%      (guard (all (<=p) (x:ys) && all (p<=) zs) >> return (x:ys, zs)) `mplus`
%      (guard (all (<=p) ys && all (p<=) (x:zs)) >> return (ys, x:zs))
%  ===       {- definition of |all|, \eqref{eq:guard-conj-split} -}
%    split xs >>= \(ys, zs) ->
%     (guard (all (<=p) ys && all (p<=) zs) >> guard (x<=p) >> return (x:ys, zs)) `mplus`
%     (guard (all (<=p) ys && all (p<=) zs) >> guard (p<=x) >> return (ys, x:zs))
%  ===       {- commutativity of |guard|, \eqref{eq:guard-conj-split} -}
%    split xs >>= \(ys, zs) -> guard (all (<=p) ys && all (p<=) zs) >>
%     ((guard (x<=p) >> return (x:ys, zs)) `mplus` (guard (p<=x) >> return (ys, x:zs)))
%  `spe`   {- inductive assumption -}
%    return (partition p xs) >>= \(ys,zs) ->
%    ((guard (x<=p) >> return (x:ys, zs)) `mplus` (guard (p<=x) >> return (ys, x:zs)))
%  `spe`   {- refining |guard| to branching -}
%    return (  let (ys,zs) = partition p xs
%              in if x <= p then (x:ys, zs) else (ys, x:zs)) {-"~~."-}
% \end{code}
% The last step is where resolved the tie: when |x| and |p| equal, the specification allows us to place |x| in either partition, and we chose the left partition for no particular reason.
% We therefore conclude:
% \begin{code}
% partition p []      =  ([],[])
% partition p (x:xs)  =  let (ys,zs) = partition p xs
%                        in if x <= p then (x:ys, zs) else (ys, x:zs) {-"~~."-}
% \end{code}
