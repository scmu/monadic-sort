%if False
\begin{code}
{-# LANGUAGE TypeOperators #-}
module Spec where

import Prelude hiding (any)
import GHC.Base (Alternative(..))
import Control.Monad

import Common
import Monads
\end{code}
%endif

\section{Specification}
\label{sec:spec}

We are now ready to present a monadic specification of sorting.
Bird~\cite{Bird:96:Functional} demonstrated how to derive various sorting algorithms from relational specifications.
In Section \ref{sec:quicksort} and \ref{sec:iqsort} we show how quicksort can be derived in our monadic calculus.

We assume a type |Elm| (for ``elements'') associated with a total preorder |(<=)|.
To sort a list |xs :: List Elm| is to choose, among all permutation of |xs|, those that are sorted:
\begin{code}
slowsort :: MonadPlus m => List Elm -> m (List Elm)
slowsort = perm >=> filt sorted {-"~~,"-}
\end{code}
where |perm :: MonadPlus m => List a -> m (List a)| non-deterministically computes a permutation of its input, |sorted :: List Elm -> Bool| checks whether a list is sorted, and |filt p x| returns |x| if |p x| holds, and fails otherwise:
\begin{code}
filt :: MonadPlus m => (a -> Bool) -> a -> m a
filt p x = guard (p x) >> return x {-"~~."-}
\end{code}
The function |guard b = if b then return () else mzero| is standard.
%
%We now proceed to define |sorted| and |perm|.
The predicate |sorted :: List Elm -> Bool| can be defined by:
% sorted :: List Elm -> Bool
\begin{code}
sorted []      = True
sorted (x:xs)  = all (x<=) xs && sorted xs {-"~~."-}
\end{code}
The following property can be proved by a routine induction on |ys|:
\begin{equation}
\begin{split}
&  |sorted (ys ++ [x] ++ zs)| ~\equiv~ \\
&\quad    |sorted ys && sorted zs && all (<=x) ys && all (x<=) zs|  \mbox{~~.}
\end{split}
    \label{eq:sorted-cat3}
\end{equation}

Now we consider the permutation phase.
As shown by Bird~\cite{Bird:96:Functional}, what sorting algorithm we end up deriving is often driven by how the permutation phase is performed.
The following definition of |perm|, for example:
\begin{spec}
perm []      = return []
perm (x:xs)  = perm xs >>= insert x {-"~~,"-}
\end{spec}
where |insert x xs| non-deterministically inserts |x| into |xs|,
would lead us to insertion sort.
% The following definition, for example, would lead us to insertion sort:
% %format perm1 = perm
% \begin{code}
% insert :: MonadPlus m => a -> List a -> m (List a)
% insert x []      = return [x]
% insert x (y:xs)  = return (x:y:xs) `mplus` ((y:) <$> insert x xs) {-"~~,"-}
%
% perm1 :: MonadPlus m => List a -> m (List a)
% perm1 []      = return []
% perm1 (x:xs)  = perm1 xs >>= insert x {-"~~."-}
% \end{code}
To derive quicksort, we use an alternative definition of |perm|:
\begin{spec}
perm :: MonadPlus m => List a -> m (List a)
perm []      =  return []
perm (x:xs)  =  split xs >>= \(ys, zs) -> liftM2 (++ [x] ++) (perm ys) (perm zs) {-"~~."-}
\end{spec}
%if False
\begin{code}
perm :: MonadPlus m => List a -> m (List a)
perm []      =  return []
-- perm [x]     =  return [x]
perm (x:xs)  =  split xs >>= \(ys, zs) -> liftM2 (\us vs -> us ++ [x] ++ vs) (perm ys) (perm zs) {-"~~,"-}
\end{code}
%endif
where |liftM2 oplus m1 m2 = m1 >>= \x1 -> m2 >>= \x2 -> return (x1 `oplus` x2)|, and |split| non-deterministically splits a list.
When the input has more than one element, we split the tail into two, permute them separately, and insert the head in the middle.
The monadic function |split| is given by:
%such that |return ((ys,zs)) `sse` split xs| if |xs| can be constructed by interleaving |ys| and |zs|:
\begin{code}
split :: MonadPlus m => List a -> m (List a :* List a)
split []      =  return (([],[]))
split (x:xs)  =  split xs >>= \(ys, zs) -> return ((x:ys, zs)) `mplus` return ((ys, x:zs)) {-"~~."-}
\end{code}
%if False
% \begin{code}
% split :: MonadPlus m => [a] -> m ([a], [a])
% split []      =  return ([],[])
% split (x:xs)  =  split xs >>= \(ys, zs) ->
%                  return (x:ys, zs) `mplus` return (ys, x:zs) {-"~~."-}
% \end{code}
%endif
%For example, |split [1,2,3]| has the following outputs: |([1,2,3],[])|, |([2,3],[1])|, |([1,3],[2])|, |([3],[1,2])|, |([1,2],[3])|, |([2],[1,3])|, |([1],[2,3])|, and |([],[1,2,3])|.
%
%The aim is to construct |qsort| such that |return . qsort `sqse` slowsort|.
%
This completes the specification.
One may argue that the second definition of |perm| is not one that,
as stated in Section~\ref{sec:intro},
``obviously'' implied by the problem description.
Bird~\cite{Bird:96:Functional} derived the second one from the first in a relational setting, and we can also show that the two definitions are equivalent.

% The aim now is to construct a function |qsort| such that is a refinement of |slowsort|.

% Simple properties about |guard|:
% \begin{align}
%   |guard p `sqse` guard q| &\Leftarrow |(p <== q)| \mbox{~~,}
%      \label{eq:guard-weaken}\\
%   |guard (p && q)| &\equiv |guard p >> guard q| \mbox{~~.}
% \end{align}
