%if False
\begin{code}
{-# LANGUAGE TypeOperators, FlexibleContexts #-}
module IQSort where

import Prelude hiding (any, read, readList)
import Control.Monad
import Data.Array

import Common
import Monads
import Spec
import QSort
\end{code}
%endif

%format (Triple (a) (b) (c)) = "(" a "\times" b "\times" c ")"
%format T3 x = x "^3"

\section{Quicksort on Arrays}
\label{sec:iqsort}

One of the advantages of using a monadic calculus is that we can integrate effects other than non-determinism into the program we derive.
In this section we derive an imperative quicksort on arrays, based on previously established properties.

% The usual presentation of the \emph{state} effect involves two operators:
% \begin{spec}
% class Monad m => MonadState s m where
%     get :: m s
%     put :: s -> m () {-"~~,"-}
% \end{spec}
% where |s| is the type of the state.
% The operator |get| fetches the value of the current state, while |put| updates the state.
% They are supposed to satisfy a set of laws~\cite{GibbonsHinze:11:Just} not presented here, but similar to the laws for our |read| and |write| in Section~\ref{sec:array-operations}.

% Suppose that we have a monadic function |f :: s -> m s|, and wish to derive a variant |h :: MonadState s m => () -> m ()| that computes |f| but reads from and writes to the state. The requirement of |h| can be written as that for all |st :: s|:
% \begin{spec}
% put st >>= h {-"~"-}`sse`{-"~"-} f st >>= put {-"~~."-}
% \end{spec}
% That is, when |h| is run with initial state |st|, it computes a value |f st| would return, and writes the result to the state. However, that is equivalent to:
% \begin{spec}
%   h {-"~"-}`sqse`{-"~"-} put \\ (f >=> put)  {-"~~."-}
% \end{spec}
% Therefore, given |f|, deriving its state-based variant is to calculate a refinement of |put \\ (f >=> put)|. Notice how the denominator (|put|) in the factor specifies the precondition, while the numerator (|f >=> put|) specifies the postcondition. This is a pattern we will see a lot in this section.

\subsection{Operations on Arrays}
\label{sec:array-operations}

% In this section we introduce the operations on arrays, encapsulated in a state monad, to be used in the sorting algorithm.
We assume that our state is an |Int|-indexed, unbounded array containing elements of type |e|, with two operations that, given an index, respectively read from and write to the array:
\begin{spec}
class Monad m => MonadArr e m where
  read   :: Int -> m e
  write  :: Int -> e -> m () {-"~~."-}
\end{spec}
They are assumed to satisfy the following laws:
\begin{align*}
  &\mbox{\bf read-write:} &
  |read i >>= write i| ~&=~ |return (())| \mbox{~~,}\\
  &\mbox{\bf write-read:} &
  |write i x >> read i| ~&=~
  |write i x >> return x| \mbox{~~,}\\
  &\mbox{\bf write-write:}&
  |write i x >> write i x'| ~&=~ |write i x'| \mbox{~~,}\\
  &\mbox{\bf read-read:}  &
  \multispan2{|read i >>= \x -> read i >>= \x' -> f x x' |~=}\\
  && \multispan2{\qquad|read i >>= \x -> f x x| \mbox{~~.}\hfil}
\end{align*}
Furthermore, we assume that (1) |read i| and |read j| commute; (2) |write i x| and |write j y| commute if |i /= j|; (3) |write i x| and |read j| commute if |i /= j|.
% \begin{align*}
%   |read i| &\mbox{~and~} |read j| \mbox{~commute,}\\
%   |write i x| &\mbox{~and~} |write j y| \mbox{~commute if |i /= j|,}\\
%   |write i x| &\mbox{~and~} |read j| \mbox{~commute if |i /= j|.}
% \end{align*}

\begin{figure}[t]
\hspace{-1cm}
{\small
\begin{spec}
readList :: MonadArr e m => Int -> Nat -> m (List e)
readList i 0      = return []
readList i (1+k)  = liftM2 (:) (read i) (readList (i + 1) k) {-"~~,"-}

writeList :: MonadArr e m => Int -> List e -> m ()
writeList i []      = return (())
writeList i (x:xs)  = write i x >> writeList (i + 1) xs {-"~~,"-}

writeL    i xs          = writeList i xs >> return (length xs) {-"~~,"-}
write2L   i (xs,ys)     = writeList i (xs ++ ys) >> return ((length xs, length ys)) {-"~~,"-}
write3L   i (xs,ys,zs)  = writeList i (xs ++ ys ++ zs) >> return ((length xs, length ys, length zs)) {-"~~."-}

swap i j = read i >>= \x -> read j >>= \y -> write i y >> write j x {-"~~."-}
\end{spec}
} %small
% \begin{minipage}{0.5\textwidth}
% {\small
% \begin{spec}
% \end{spec}
% %} %small
% \end{minipage}
% \begin{minipage}{0.5\textwidth}
% {\small
% \begin{spec}
% \end{spec}
% } %small
% \end{minipage}
\vspace{-0.7cm}
\caption{Operations for reading and writing chunks of data.}
\label{fig:readWriteList}
\end{figure}
% Defined for particular instances in Common.hs
% if False
% \begin{code}
% readList :: MonadArr m => Int -> Nat -> m (List a)
% readList i 0      = return []
% readList i k  = liftM2 (:) (read i) (readList (i + 1) (k-1)) {-"~~,"-}
%
% writeList :: MonadArr m => Int -> List a -> m ()
% writeList i []      = return (())
% writeList i (x:xs)  = write i x >> writeList (i + 1) xs {-"~~,"-}
% \end{code}
% endif
More operations defined in terms of |read| and |write| are shown in Figure~\ref{fig:readWriteList}, where |length xs| abbreviates |{-"\Varid{length}~"-}xs|.
The function |readList i n|, where |n| is a natural number, returns a list containing the |n| elements in the array starting from index |i|. Conversely, |writeList i xs| writes the list |xs| to the array with the first element being at index |i|.
In imperative programming we often store sequences of data into an array and return the length of the data. Thus, functions
|writeL|, |write2L| and |write3L| store lists into the array before returning their lengths.
These |read| and |write| family of functions are used only in the specification; the algorithm we construct should only mutate the array by |swap|ing elements.

Among the many properties of |readList| and |writeList| that can be induced from their definitions, the following will be used in a number of crucial steps:
\begin{equation}
  |writeList i (xs ++ ys)| ~=~ |writeList i xs >> writeList (i + length xs) ys| \mbox{~~.} \label{eq:writeList-++}
\end{equation}
%if False
\begin{code}
writeListApp :: MonadArr e m => Int -> List e -> List e -> m ()
writeListApp i xs ys =
  writeList i (xs ++ ys) === writeList i xs >> writeList (i + length xs) ys
\end{code}
%endif

%if False
\begin{code}
writeL :: MonadArr e m => Int -> List e -> m Nat
writeL   i xs          = writeList i xs >> return (length xs) {-"~~,"-}

write2L :: MonadArr e m => Int -> ([e], [e]) -> m (Nat, Nat)
write2L  i (xs,ys)     = writeList i (xs ++ ys) >> return ((length xs, length ys)) {-"~~,"-}

write3L :: MonadArr e m => Int -> ([e], [e], [e]) -> m (Nat, Nat, Nat)
write3L  i (xs,ys,zs)  = writeList i (xs ++ ys ++ zs) >> return ((length xs, length ys, length zs)) {-"~~."-}
\end{code}
%endif

A function |f :: List a -> m (List a)| is said to be \emph{length preserving} if
|f xs >>= \ys -> return ((ys, length ys))| |=|
|f xs >>= \ys -> return ((ys, length xs))|.
% \begin{equation*}
%   |f xs >>= \ys -> return ((ys, length ys))| ~=~
%     |f xs >>= \ys -> return ((ys, length xs))| \mbox{~~.}
% \end{equation*}
It can be proved that |perm|, and thus |slowsort|, are length preserving.

\paragraph{On ``composing monads''}
In the sections to follow, some readers may have concern seeing |perm|, having class constraint |MonadPlus m|, and some other code having constraint |MonadArr e m| in the same expression.
This is totally fine:
mixing two such subterms simply results in an expression having constraint |(MonadPlus m, MonadArr e m)|.
No |lift|ing is necessary.

We use type classes to make it clear that we do not specify what exact monad |perm| is implemented with.
It could be one monolithic monad, a monad built from monad transformers~\cite{MTL:14}, or a free monad interpreted by effect handlers~\cite{KiselyovIshii:15:Freer}.
% It could be the Set monad, or could be a monad that supports both non-determinism and array operations (the latter not used in |perm|).
% In the latter case, |perm| can be mixed with stateful code.
All theorems and derivations about |perm| hold regardless of the actual monad, as long as the monad satisfies all properties we demand.

\subsection{Partitioning an Array}

While the list-based |partition| is relatively intuitive,
partitioning an array \emph{in-place} (that is, using at most $O(1)$ additional space) is known to be a tricky phase of array-based quicksort.
Therefore we commence our discussion from deriving in-place array partitioning  from the list version.
The partition algorithm we end up deriving is known as the \emph{Lomuto scheme}~\cite{Bentley:00:Programming}, as opposed to Hoare's~\cite{Hoare:61:Partition}.

\subsubsection*{Specification}
There are two issues to deal with before we present a specification for an imperative, array-based partitioning, based on list-based |partition|.
Firstly, |partition| is not tail-recursive, while many linear-time array algorithms are implemented as a tail-recursive $\Conid{for}$-loop.
Thus we apply the standard trick constructing a tail-recursive algorithm by introducing accumulating parameters.
Define (we write the input/outputs of |partition| in bold font for clarity):
\begin{spec}
partl :: Elm -> (List Elm :* List Elm :* List Elm) -> (List Elm :* List Elm)
partl p (ys, zs, bold xs) =  let (bold us, bold vs) = partition p (bold xs)
                             in (ys++ bold us, zs++ bold vs) {-"~~."-}
\end{spec}
%where we write the input/outputs of |partition| in bold font for clarity.
In words, |partl p (ys, zs, bold xs)| partitions |bold xs| into |(bold us, bold vs)| with respect to pivot |p|, but appends |ys| and |zs| respectively to |bold us| and |bold vs|.
It is a generalisation of |partition| because |partition p xs = partl p ([],[],xs)|.
By routine calculation exploiting associativity of |(++)|, we can derive a tail-recursive definition of |partl|:
%format bxs = "\bm{\Varid{xs}}"
%format bx = "\bm{\Varid{x}}"
%format bus = "\bm{\Varid{us}}"
%format bvs = "\bm{\Varid{vs}}"
\begin{code}
partl p (ys, zs, [])      = (ys, zs)
partl p (ys, zs, bx:bxs)  = if bx <= p  then  partl p (ys++[bx], zs, bxs)
                                        else  partl p (ys, zs++[bx], bxs) {-"~~."-}
\end{code}
It might aid our understanding if we note that, if we start |partl| with initial value |([],[],xs)| we have the invariant that |ys| contains elements that are at most |p|, and elements in |zs| are larger than |p|. The calculations below, however, do not rely on this observation.
\footnote{It might be worth noting that |partl| causes a space
leak in Haskell, since the accumulators become thunks
that increase in size as the input list is traversed.
It does not matter here since |partl| merely serves as a specification of |ipartl|.}

Our wish is to construct a variant of |partl| that works on arrays.
That is, when the array contains |ys ++ zs ++ bold xs|, the three inputs to |partl| in a consecutive segment, when the derived program finishes its work we wish to have |ys ++ bold us ++ zs ++ bold vs|, the output of |partl|, stored consecutively in the array.

This brings us to the second issue: |partition|, and therefore |partl|, are stable (that is, elements in each partition retain their original order), which is a strong requirement for array-based partitioning.
It is costly to mutate |ys ++ zs ++ bold xs| into |ys ++ bold us ++ zs ++ bold vs|, since it demands that we retain the order of elements in |zs| while inserting elements of |bold us|.
For sorting we do not need such a strong postcondition.
It is sufficient, and can be done more efficiently, to mutate |ys ++ zs ++ bold xs| into |ys ++ bold us ++ ws|, where |ws| is some permutation of |zs ++ bold vs|.
% as long as |ws| contains each element in |xs| that is at least |p|.
It is handy allowing non-determinism: we introduce a |perm| in our specification, indicating that we do not care about the order of elements in |ws|.

Define |second :: Monad m => (b -> m c) -> (a,b) -> m (a,c)|,
which applies a monadic function to the second component of a tuple:
\begin{code}
second f (x,y) = f y >>= \y' -> return ((x,y')) {-"~~."-}
\end{code}
%if False
\begin{code}
snd3 :: Monad m => (b -> m d) -> (a,b,c) -> m (a,d,c)
snd3 f (x,y,z) = f y >>= \y' -> return ((x,y',z)) {-"~~."-}
\end{code}
%endif
% \begin{spec}
% snd3 :: Monad m => (b -> m d) -> (a:* b:* c) -> m (a:* d :*c)
% snd3 f (x,y,z) = f y >>= \y' -> return ((x,y',z)) {-"~~."-}
% \end{spec}
Our new wish is to construct an array counterpart of |second perm . partl p|.
Let the function be
\begin{spec}
ipartl :: (MonadPlus m, MonadArr Elm m) =>
    Elm -> Int -> (Nat :* Nat :* Nat) -> m (Nat :* Nat) {-"~~."-}
\end{spec}
The intention is that in a call |ipartl p i (ny,nz,nx)|, |p| is the pivot, |i| the index where |ys++zs++bxs| is stored in the array, and |ny|, |nz|, |nx| respectively the lengths of |ys|, |zs|, and |xs|. A specification of |ipartl| is:
%if False
\begin{code}
ipartlSpec :: (MonadArr Elm m, MonadPlus m) =>
  Int -> [Elm] -> [Elm] -> [Elm] -> Elm -> m (Int, Int)
ipartlSpec i ys zs bxs p =
\end{code}
%endif
\begin{code}
 writeList i (ys++zs++ bxs) >> ipartl p i (length ys, length zs, length bxs) `sse`
   second perm (partl p (ys, zs, bxs)) >>= write2L i {-"~~."-}
\end{code}
That is, under assumption that |ys ++ zs ++ bxs| is stored in the array starting from index |i| (initialised by |writeList|), |ipartl| computes |partl p (ys,zs,bxs)|, possibly permuting the second partition.
The resulting two partitions are still stored in the array starting from |i|, and their lengths are returned.

\subsubsection{Derivation}
We start with fusing |second perm| into |partl|,
that is, to construct |partl' p `sqse` second perm . partl p|.%
\footnote{We will discover a stronger specification |partl' p `sqse` snd3 perm \\ (second perm . partl p)|,
where |snd3 f (x,y,z) = f y >>= \y' -> return ((x,y',z))|.
We omit the details.
}
If we discover an inductive definition of |partl'|, it can then be used to construct an inductive definition of |ipartl|.
%
% That is, to construct |partl' :: MonadPlus m => Elm -> (List Elm :* List Elm :* List Elm) -> m (List Elm :* List Elm)| such that
% \begin{equation}
% |snd3 perm (ys, zs, xs) >>= partl' p| ~\subseteq~ |second perm (partl p (ys, zs, xs))| \mbox{~~.} \label{eq:partl'-spec}
% \end{equation}
% Equivalently, |partl' p `sqse` snd3 perm \\ (snd3 perm . partl p)|.
%
With some routine calculation we get:
%if false
% \begin{code}
% partl' :: MonadPlus m => Elm -> (List Elm, List Elm, List Elm) -> m (List Elm, List Elm)
% \end{code}
%endif
\begin{code}
partl' :: MonadPlus m => Elm -> T3 (List Elm) -> m (List Elm :* List Elm)
partl' p (ys, zs, [])      = return ((ys, zs))
partl' p (ys, zs, bx:bxs)  =
  if bx <= p  then  perm zs >>= \zs' -> partl' p (ys ++ [bx], zs', bxs)
              else  perm (zs ++ [bx]) >>= \zs' -> partl' p (ys, zs', bxs) {-"~~."-}
\end{code}
% if x < p  then  perm zs >>= \zs' -> partl' p (ys ++ [x], zs', xs)
%           else  perm (zs ++ [x]) >>= \zs' -> partl' p (ys, zs', xs) {-"~~."-}
% (  return (if bx<=p then (ys++[bx], zs, bxs) else (ys,zs++[bx], bxs)) >>=
%    snd3 perm) >>= partl' p {-"~~."-}
For an intuitive explanation, rather than permuting the second list |zs| after computing |partl|, we can also permute |zs| in |partl'| before every recursive call.
% In this calculation we will need properties including
% \begin{align*}
% |perm xs| ~&=~ |perm xs >>= perm| \mbox{~~,}\\
% |perm (xs++[x])| ~&=~ |perm xs >>= \xs' -> perm (xs'++[x])|  \mbox{~~.}
% \end{align*}

The specification of |ipartl| now becomes
\begin{equation}
\begin{split}
&|writeList i (ys++zs++bxs) >> ipartl p i (length ys, length zs, length bxs) `sse`| \\
&\qquad   |partl' p (ys, zs, bxs) >>= write2L i| \mbox{~~.}
   \label{eq:ipartl-spec}
\end{split}
\end{equation}
To calculate |ipartl|, we start with the right-hand side of |(`sse`)|,
%|partl' p (ys, zs, xs) >>= write2L i|,
since it contains more information to work with.
We try to push |write2L| leftwards until the expression has the form |writeList i (ys++zs++xs) >> ...|, thereby constructing |ipartl|.
This is similar to that, in imperative program calculation, we {\em work backwards from the postcondition} to construct a program that works under the given precondition~\cite{Dijkstra:76:Discipline}.

We intend to construct |ipartl| by induction on |xs|.
For |xs := []|, we get |ipartl p i| |(ny, nz, 0) = return ((ny, nz))|.
For the case |x:xs|, assume that the specification is met for |xs|.
Just for making the calculation shorter, we refactor |partl'|, lifting the recursive calls and turning the main body into an auxiliary function:
%if False
\begin{code}
partl'Dispatch :: MonadPlus m =>
  Elm -> List Elm -> List Elm -> Elm -> [Elm] -> m (List Elm :* List Elm)
partl'Dispatch p ys zs bx bxs =
\end{code}
%endif
\begin{code}
 partl' p (ys, zs, bx:bxs) === dispatch bx p (ys,zs,bxs) >>= partl' p {-"~~,"-}
  where dispatch bx p (ys,zs,bxs) =
         if bx <= p  then  perm zs >>= \zs' -> return ((ys ++ [bx], zs', bxs))
                     else  perm (zs ++ [bx]) >>= \zs' -> return ((ys, zs', bxs)) {-"~~."-}
\end{code}
% where dispatch bx p (ys,zs,bxs) =
%   if bx <= p then (ys ++ [bx], zs,bxs) else (ys, zs ++ [bx], bxs) {-"~~."-}
% \begin{spec}
% partl' p (ys, zs, bx:bxs) = snd3 perm (dispatch bx p (ys,zs,bxs)) >>= partl' p {-"~~,"-}
%   where dispatch bx p (ys,zs,bxs) =
%     if bx <= p then (ys ++ [bx], zs,bxs) else (ys, zs ++ [bx], bxs) {-"~~."-}
% \end{spec}
%if False
\begin{code}
-- dispatch x p (ys,zs,xs) =
--   if x < p then (ys ++ [x], zs,xs) else (ys, zs ++ [x], xs)
dispatch :: (Ord a, MonadPlus m) =>
  a -> a -> ([a], List a, c) -> m ([a], List a, c)
dispatch bx p (ys,zs,xs) =
  if bx <= p  then  perm zs >>= \zs' -> return ((ys ++ [bx], zs', bold xs))
              else  perm (zs ++ [bx]) >>= \zs' -> return ((ys, zs', bold xs)) {-"~~."-}
\end{code}
%endif
We calculate:
%if False
\begin{code}
ipartl_der_1 :: (MonadPlus m, MonadArr Elm m) => Elm -> [Elm] -> List Elm -> Elm -> [Elm] -> Int -> m (Nat :* Nat)
ipartl_der_1 p ys zs bx bxs i =
\end{code}
%endif
\begin{code}
    partl' p (ys, zs, bx:bxs) >>= write2L i
 ===       {- definition of |partl'| -}
    (dispatch bx p (ys,zs,bxs) >>= partl' p) >>= write2L i
 `spe`     {- monad laws, inductive assumption -}
    (dispatch bx p (ys,zs,bxs) >>= write3L i) >>= ipartl p i
 ===       {- by \eqref{eq:writeList-++}, monad laws -}
    dispatch bx p (ys,zs,bxs) >>= \(ys', zs', bxs) ->
    writeList i (ys'++zs') >> writeList (i+length (ys'++zs')) bxs >>
    ipartl p i (length ys', length zs', length bxs)
 ===       {- |perm| preserves length, commutativity -}
    writeList (i+length ys+length zs+1) bxs >>
    dispatch bx p (ys,zs,bxs) >>= \(ys', zs', bxs) ->
    writeList i (ys'++zs') >>
    ipartl p i (length ys', length zs', length bxs)
 ===       {- definition of |dispatch|, function calls distribute into |if| -}
   writeList (i+length ys+length zs+1) bxs >>
   if bx <= p  then  perm zs >>= \zs' -> writeList i (ys++[bx]++zs') >>
                     ipartl p i (length ys+1, length zs', length bxs)
               else  perm (zs++[bx]) >>= \zs' -> writeList i (ys++zs') >>
                     ipartl p i (length ys, length zs', length bxs) {-"~~."-}
\end{code}

We pause here to see what has happened: we have constructed a precondition |writeList (i+length ys+length zs+1) bxs|, which is part of the desired precondition: |writeList i (ys++zs++(bx:bxs))|.
To recover the latter precondition, we will try to turn both branches of |if| into the form |writeList i (ys++zs++[bx]) >>= ... |. That is, we try to construct, in both branches, some code that executes under the precondition |writeList i (ys++zs++[bx])| --- that the code generates the correct result is guaranteed by the refinement relation.

It is easier for the second branch, where we can simply refine |perm| to |return|:
%if False
\begin{code}
ipartl_der_2 :: (MonadPlus m, MonadArr Elm m) => Elm -> [Elm] -> [Elm] -> Elm -> Int -> List Elm -> m (Nat, Nat)
ipartl_der_2 p ys zs bx i bxs =
\end{code}
%endif
\begin{code}
       perm (zs++[bx]) >>= \zs' -> writeList i (ys++zs') >>
       ipartl p i (length ys, length zs', length bxs)
 `spe`    {- since |return xs `sse` perm xs| -}
       writeList i (ys++zs++[bx]) >> ipartl p i (length ys, length zs +1, length bxs) {-"~~."-}
\end{code}

For the first branch, we focus on its first line:
%if False
\begin{code}
ipartl_der_3 :: (MonadPlus m, MonadArr Elm m) => [Elm] -> [Elm] -> Elm -> Int -> m ()
ipartl_der_3 ys zs bx i =
\end{code}
%endif
\begin{code}
   perm zs >>= \zs' -> writeList i (ys++[bx]++zs')
 ===      {- by \eqref{eq:writeList-++}, commutativity -}
   writeList i ys >> perm zs >>= \zs' -> writeList (i+length ys) ([bx]++zs')
 `spe`    {- introduce |swap|, see below -}
   writeList i ys >> writeList (i+length ys) (zs++[bx]) >>
   swap (i+length ys) (i+length ys+length zs)
 ===      {- by \eqref{eq:writeList-++} -}
   writeList i (ys++zs++[bx]) >> swap (i+length ys) (i+length ys+length zs) {-"~~."-}
\end{code}
Here we explain the last two steps.
Operationally speaking, given an array containing |ys++zs++[bx]| (the precondition we wanted, initialized by the |writeList| in the last line), how do we mutate it to |ys++[bx]++zs'| (postcondition specified by the |writeList| in the first line), where |zs'| is a permutation of |zs|? We may do so by swapping |bx| with the leftmost element of |zs|, which is what we did in the second step. Formally, we used the property:
\begin{equation}
\begin{split}
&|perm zs >>= \zs' -> writeList i ([bx]++zs') `spe`|\\
&\qquad|writeList i (zs++[bx]) >> swap i (i+length zs) {-"~~."-}|
\end{split} \label{eq:perm-write-swap}
\end{equation}

Now that both branches are refined to code with precondition |writeList i (ys++zs++[bx])|, we go back to the main derivation:
%if False
\begin{code}
ipartl_der_4 :: (MonadPlus m, MonadArr Elm m) =>
       Elm -> List Elm -> List Elm -> Elm ->
                List Elm -> Int -> m (Int, Int)
ipartl_der_4 p ys zs bx bxs i =
\end{code}
%endif
\begin{code}
  writeList (i+length ys+length zs+1) bxs >>
  if bx <= p  then  writeList i (ys++zs++[bx]) >>
                    swap (i+length ys) (i+length ys+length zs) >>
                    ipartl p i (length ys+1, length zs, length bxs)
              else  writeList i (ys++zs++[bx]) >>
                    ipartl p i (length ys, length zs+1, length bxs)
 ===   {- distributivity of |if|, \eqref{eq:writeList-++} -}
  writeList i (ys++zs++(bx:bxs)) >>
  if bx <= p  then  swap (i+length ys) (i+length ys+length zs) >>
                    ipartl p i (length ys+1, length zs, length bxs)
              else  ipartl p i (length ys, length zs+1, length bxs)
 ===   {- {\bf write-read} and definition of |writeList| -}
  writeList i (ys++zs++(bx:bxs)) >>
  read (i+length ys+length zs) >>= \bx ->
  if bx <= p  then  swap (i+length ys) (i+length ys+length zs) >>
                    ipartl p i (length ys+1, length zs, length bxs)
              else  ipartl p i (length ys, length zs+1, length bxs) {-"~~."-}
\end{code}
We have thus established the precondition |writeList i (ys++zs++(bx:bxs))|.
In summary, we have derived:
\begin{spec}
ipartl :: MonadArr Elm m => Elm -> Int -> (Int:* Int:* Int) -> m (Int:* Int)
ipartl p i (ny, nz, 0)    = return ((ny, nz))
ipartl p i (ny, nz, 1+k)  =
  read (i+ny+nz) >>= \x ->
  if x<=p  then  swap (i+ny) (i+ny+nz) >> ipartl p i (ny+1, nz, k)
           else  ipartl p i (ny, nz+1, k) {-"~~."-}
\end{spec}
%if False
\begin{code}
ipartl :: MonadArr Elm m => Elm -> Int -> (Int, Int, Int) -> m (Int, Int)
ipartl p i (ny, nz, 0)    = return ((ny, nz))
ipartl p i (ny, nz, k)  =
  read (i+ny+nz) >>= \x ->
  if x<=p  then  swap (i+ny) (i+ny+nz) >> ipartl p i (ny+1, nz, k-1)
           else  ipartl p i (ny, nz+1, k-1) {-"~~."-}
\end{code}
%endif

\subsection{Sorting an Array}

Now that we have |ipartl| derived, the rest of the work is to install it into quicksort.
We intend to derive |iqsort :: MonadArr Elm m => Int -> Nat -> m ()| such that |isort i n| sorts the |n| elements in the array starting from index |i|.
We can give it a formal specification:
\begin{equation}
|writeList i xs >> iqsort i (length xs)| ~\subseteq~
  |slowsort xs >>= writeList i| \mbox{~~.}\label{eq:iqsort-spec}
\end{equation}
% \begin{equation*}
%   |iqsort i| ~\sqsubseteq~ |writeL i \\ (slowsort >=> writeList i)| \mbox{~~.}
% \end{equation*}
% %if False
% \begin{code}
% iqsortSpec1 :: (MonadArr Elm m, MonadPlus m) => Int -> Nat -> m ()
% iqsortSpec1 i =
%   iqsort i `sqse` writeL i \\ (slowsort >=> writeList i)
% \end{code}
% %endif
That is, when |iqsort i| is run from a state initialised by |writeList i xs|, it should behave the same as |slowsort xs >>= writeList i|.
%if False
\begin{code}
iqsortSpec2 :: (MonadArr Elm m, MonadPlus m) => Int -> List Elm -> m ()
iqsortSpec2 i xs =
  writeList i xs >> iqsort i (length xs)  `sse`
     slowsort xs >>= writeList i
\end{code}
%endif

The function |iqsort| can be constructed by induction on the length of the input list.
For the case |xs := p:xs|, we start from the left-hand side
|slowsort (p:xs) >>= writeList i| and attempt to transform it to
|writeList i (p:xs) >> ...|, thereby construct |iqsort|.
We present only the hightlights of the derivation.
Firstly, |slowsort (p:xs) >>= writeList i| can be transformed to:
\begin{spec}
partl' p ([], [], xs) >>= \(ys, zs) ->
perm ys >>= \ys' -> writeList i (ys'++[p]++zs) >>
iqsort i (length ys) >> iqsort (i+length ys+1) (length zs) {-"~~."-}
\end{spec}
% We start from the right-hand side and, according to definition of |slowsort|, consider |xs := []|, |xs := [x]|, and the case when |xs| is a list having at least two elements.
% For the first two cases, we get |isort i 0 = isort i 1 = return (())|.
% For the last case, we proceed below in bigger steps, assuming that \eqref{eq:iqsort-spec} has been met for lists shorter than |xs|:
%if False
\begin{code}
iqsort_der1 :: (MonadPlus m, MonadArr Elm m) =>
   Elm -> [Elm] -> Int -> m ()
iqsort_der1 p xs i =
\end{code}
\begin{code}
    slowsort (p:xs) >>= writeList i
 `spe`  {- \eqref{eq:slowsort-rec} -}
    let (ys, zs) = partition p xs
    in  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
        writeList i (ys'++[p]++zs')
 ===       {- since |perm >=> slowsort = slowsort| -}
    let (ys, zs) = partition p xs
    in  perm ys >>= \ys'' -> perm zs >>= \zs'' ->
        slowsort ys'' >>= \ys' -> slowsort zs'' >>= \zs' ->
        writeList i (ys'++[p]++zs')
 ===       {-  specifications of |partl| and |partl'| -}
    partl' p ([], [], xs) >>= \(ys, zs) ->
    perm ys >>= \ys'' ->
    slowsort ys'' >>= \ys' -> slowsort zs >>= \zs' ->
    writeList i (ys'++[p]++zs')
 `spe`   {- Lemma~\ref{lma:slowsort-split}, |perm| preserves length -}
    partl' p ([], [], xs) >>= \(ys, zs) ->
    perm ys >>= \ys' -> writeList i (ys'++[p]++zs) >>
    iqsort i (length ys) >> iqsort (i+length ys+1) (length zs) {-"~~."-}
\end{code}
%endif
For that to work, we introduced two |perm| to permute both partitions generated by |partition|. We can do so because |perm >=> perm = perm| and thus |perm >=> slowsort = slowsort|. The term |perm zs| was combined with |partition p|, yielding |partl' p|, while |perm ys| will be needed later.
We also needed \eqref{eq:writeList-++} to split |writeList i (ys'++[x]++zs')| into two parts. Assuming that \eqref{eq:iqsort-spec} has been met for lists shorter than |xs|, two subexpressions are folded back to |iqsort|.

% In the last step we replace the two occurrences of |slowsort| by |iqsort|.
% It is clearer if we make that step a lemma:
% \begin{lemma}\label{lma:slowsort-split}
% If~\eqref{eq:iqsort-spec} holds for |xs:=ys| and |(i,xs):=(i+length ys +1, zs)|, we have that for all |p|:
% %if False
% \begin{code}
% slowsort_split :: (MonadPlus m, MonadArr Elm m) =>
%   List Elm -> List Elm -> Int -> Elm -> m ()
% slowsort_split ys zs i p =
% \end{code}
% %endif
% \begin{code}
%  slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
%    writeList i (ys'++[p]++zs') {-"~~"-}`spe`
%      writeList i (ys++[p]++zs) >>
%       iqsort i (length ys) >> iqsort (i+length ys+1) (length zs) {-"~~."-}
% \end{code}
% \end{lemma}
% The proof proceeds by splitting |writeList i (ys'++[x]++zs')| into parts by \eqref{eq:writeList-++}, and move the |write|s around by commutativity, in order to apply \eqref{eq:iqsort-spec}. We omit the details.

Now that we have introduced |partl'|, the next goal is to embed |ipartl|.
The status of the array before the two calls to |iqsort| is given by |writeList i (ys'++[p]++zs)|. That is, |ys'++[p]++zs| is stored in the array from index |i|, where |ys'| is a permutation of |ys|. The postcondition of |ipartl|, according to the specification \eqref{eq:ipartl-spec}, ends up with |ys| and |zs| stored consecutively. To connect the two conditions, we use a lemma that is dual to \eqref{eq:perm-write-swap}:
\begin{equation}
\begin{split}
&|perm ys >>= \ys' -> writeList i (ys' ++ [p]) `spe`|\\
&\qquad|writeList i ([p] ++ ys) >> swap i (i+length ys) {-"~~."-}|
\end{split} \label{eq:perm-write-swap-2}
\end{equation}
This is what the typical quicksort algorithm does: swapping the pivot with the last element of |ys|, and \eqref{eq:perm-write-swap-2} says that it is valid because that is one of the many permutations of |ys|. With~\eqref{eq:perm-write-swap-2} and \eqref{eq:ipartl-spec}, the specification can be refined to:
\begin{spec}
writeList i (p:xs) >>
ipartl p (i+1) (0, 0, length xs) >>= \(ny, nz) -> swap i (i+ny) >>
iqsort i (length ys) >> iqsort (i+length ys+1) (length zs) {-"~~."-}
\end{spec}
%
% We focus the first two lines of the previous calculation, omitting the two calls to |iqsort|:
% %if False
% \begin{code}
% iqsort_der2 :: (MonadPlus m, MonadArr Elm m) =>
%     Elm -> List Elm -> Int -> m ()
% iqsort_der2 p xs i =
% \end{code}
% %endif
% \begin{code}
%     partl' p ([], [], xs) >>= \(ys, zs) ->
%     perm ys >>= \ys' -> writeList i (ys'++[p]++zs)
%  `spe`  {- by \eqref{eq:writeList-++} and \eqref{eq:perm-write-swap-2} -}
%     partl' p ([], [], xs) >>= \(ys, zs) ->
%     writeList i ([p]++ys++zs) >> swap i (i+length ys)
%  ===    {- definition of |writeList|, commutativity -}
%     write i p >>
%     partl' p ([], [], xs) >>= \(ys, zs) ->
%     writeList (i+1) (ys++zs) >> swap i (i+length ys) {-"~~."-}
% \end{code}
% Now that we have established the postcondition |writeList (i+1) (ys++zs)|, we can, using \eqref{eq:ipartl-spec}, refine the expression above to
% %if False
% \begin{code}
% iqsort_der3 :: MonadArr Elm m => Elm -> List Elm -> Int -> m ()
% iqsort_der3 p xs i =
% \end{code}
% %endif
% \begin{code}
%    write i p >> writeList (i+1) xs >>
%    ipartl p (i+1) (0, 0, length xs) >>= \(ny, nz) -> swap i (i+ny)
%  ===     {- definition of |writeList|, {\bf write-read} -}
%    writeList i (p:xs) >> read i >>= \p ->
%    ipartl p (i+1) (0, 0, length xs) >>= \(ny, nz) -> swap i (i+ny)  {-"~~."-}
% \end{code}
% Note again how we work from the postcondition backwards to establish a needed precondition.

In summary, we have derived:
\begin{code}
iqsort :: MonadArr Elm m => Int -> Nat -> m ()
iqsort i 0  =  return (())
iqsort i n  =  read i >>= \p ->
               ipartl p (i+1) (0, 0, n-1) >>= \(ny, nz) ->
               swap i (i+ny) >>
               iqsort i ny >> iqsort (i+ny+1) nz {-"~~."-}
\end{code}

%if False
\begin{code}
-- run with
--   runS (iqsort 0 (length xs)) 0 xs
\end{code}
%endif


% \begin{proof} We reason:
% \begin{spec}
%        slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
%        writeList i (ys'++[x]++zs')
% =      slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->
%        writeList i ys' >> write (i+length ys) x >> writeList (i+length ys+1) zs'
% =        {- non-determinism and state commute -}
%        write (i+length ys) x >>
%        slowsort ys >>= \ys' -> writeList i ys' >>
%        slowsort zs >>= \zs' -> writeList (i+length ys+1) zs'
% `spe`    {- by~\eqref{eq:iqsort-spec} -}
%        write (i+length ys) x >>
%        slowsort ys >>= \ys' -> writeList i ys' >>
%        writeList (i+length ys+1) zs >> iqsort (i+length ys+1) (length zs)
% =        {- commutativity -}
%        write (i+length ys) x >> writeList (i+length ys+1) zs
%        slowsort ys >>= \ys' -> writeList i ys' >>
%        iqsort (i+length ys+1) (length zs)
% `spe`    {- by~\eqref{eq:iqsort-spec} -}
%        write (i+length ys) x >> writeList (i+length ys+1) zs
%        writeList i ys >> iqsort i (length ys)
%        iqsort (i+length ys+1) (length zs)
% =        {- commutativity, \eqref{eq:writeList-++} -}
%        writeList i (ys++[x]++zs) >> iqsort i (length ys) >>
%        iqsort (i+length ys+1) (length zs) {-"~~."-}
% \end{spec}
% \end{proof}
