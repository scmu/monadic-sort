\section{Introduction}

Program derivation is the technique for constructing a program from a problem specification in a stepwise manner such that every step is rigorously justified.
It is often the case that, for certain inputs to a problem, several answers are equally preferred.
Sorting is a typical example: when the array to be sorted contains items with identical keys, the order they are arranged might not matter.
Leaving the order unspecified allows more efficient algorithms.
Such problems can be modelled as non-deterministic computations from the input to all valid outputs.
The derivation is then a stepwise refinement of the specification, during which we are given flexibility to choose, according to various concerns of the algorithm being designed, which output to eventually return.
Indeed, Dijkstra~\cite{Dijkstra:76:Discipline}
%, when designing Guarded Command Language,
argued that a calculus for program derivation should take non-determinism as default and determinism as a special case.

The same applies to derivation of functional programs.
Indeed, in the 90's there was a trend generalising from functions to relations --- non-deterministic mappings between inputs and outputs.
Backhouse et al.~\cite{BackhousedeBruin:91:Relational} generalised catamorphism to relations,
and a theory of datatypes was developed basing on generalisation
from functors to relators~\cite{Backhouse:91:Polynomial}.
Bird and de Moor~\cite{BirddeMoor:97:Algebra} presented various examples of program derivation, in particular those for optimisation problems, in the category of sets and relations.
While their calculus was in practice always instantiated to relations, a large proportion of the theory was presented in a more general setting, for distributive, division, power \emph{allegories} --- that is, categories extended with operations including intersection, union, factors, and a notion of ordering used to model refinement.
%
Although the calculus was, for advocators including the authors of this paper, concise and elegant, for those who were not following this line of development, the  barrier between the relational calculus and the rest of the functional programming community was hard to overcome.
It was complained that the concept of relations is too hard, the
notations too bizarre, and reasoning with inequality (refinement) too complex.

Preceding the development of relations for program derivation, another way to model non-determinism has gained popularity.
Monads~\cite{Moggi:89:Computational} was introduced into the world of functional programming as a way to rigorously talk about side effects including IO, state, exception, and non-determinism.
Although it is also considered one of the main obstacles in learning functional programming (in particular Haskell), monads have gained relatively wide acceptance.

In this paper we propose a calculus of program derivation based on monads --- essentially moving to a Kleisli category.
While we do not claim to have established a precise parallel to allegories, many of the key ingredients of the relational approach, including union, intersection, refinement (inclusion ordering), and factor, etc., do have their counterparts in monads that can be used in program calculation.
Problem specifications can be given as non-deterministic monads, to be refined to deterministic functional programs through calculation.
One of the benefits is that functional programmers may deploy techniques they are familiar with, including pointwise reasoning and induction on structures of size of data types, when reasoning about and deriving programs.

As an additional bonus of using monads, we may talk about effects other than non-determinism. We demonstrate how to, from a specification of quicksort on lists, construct the imperative quicksort for arrays.
It is known in the community that the notion of factors is closely related to pre and postconditions.
Indeed, in our case studies, imperative programs can be modelled using factors, and their calculation mimics construction of imperative programs by reasoning backwards from postconditions.

This paper aims to be an example-driven tutorial of monadic program derivation, rather than a complete theoretical treatment.
After introducing essential operators for non-deterministic monads in Section~\ref{sec:monads},
we present our first example --- selecting a minimum element of a list, in Section~\ref{sec:minimum}.
In Section~\ref{sec:quicksort} we present a derivation of quicksort, from a general specification of sorting --- compute all permutations of a list before returning a sorted one.
We then introduce operations on mutable arrays as another effect, and show in Section~\ref{sec:iqsort} that the specification for sorting can be refined to the well-known quicksort for arrays.
In Section~\ref{sec:conclusions} we discuss the ingredients that are possibly useful but still missing, as well as related work.
