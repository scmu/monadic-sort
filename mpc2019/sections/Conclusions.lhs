\section{Conclusions and Related Work}
\label{sec:conclusions}

Through the examples presented, we hope to demonstrate that monad is a good choice as a calculus for program derivation that involves non-determinism.
It is expressive enough to model many fundamental concepts, including union, intersection, factor, and refinement, in a concise manner.
In the derivation one may switch between point-free and pointwise styles, and deploy techniques that functional programmers have familiarised themselves with, such as pattern matching and induction on structures or on sizes.
Programs having other effects can be naturally incorporated into this framework.
The pre- and post-conditions of a stateful program can be expressed in terms of factors, and the way we derive stateful programs echos how we, in Dijkstra's style, reason backwards from the postcondition.

De Moor and Gibbons~\cite{deMoorGibbons:00:Pointwise} proposed a language for non-deterministic computation that has variables, and suggested a categorial semantics.
Proposals alone this line tend to exhibit confusion when a non-deterministic value is passed to a function --- it appears that $\beta$-reduction and $\eta$-conversion does not always hold.
An example shown by de Moor and Gibbons~\cite{deMoorGibbons:00:Pointwise} is that |(\x -> x - x) (0 `mplus` 1)|, where |mplus| denotes non-deterministic choice, always yields |0|, while |(0 `mplus` 1) - (0 `mplus` 1)| could be |0|, |1|, or |-1|.
A monadic calculus sidesteps this problem by distinguishing pure and non-deterministic values by their types.
Let |m = return 0 `mplus` return 1|, the first program is written |m >>= \x -> return (x - x)| in our calculus, while the second is |m >>= \x1 -> m >>= \x2 -> return (x1 - x2)|. They are clearly different programs.

%format `mf` = "\mathbin{\backslash\!\!{\vstretch{0.3}{\hstretch{0.5}{\blacksquare}}}}"
Hoare~\cite{HoareHe:87:Weakest} pointed out the connection between Dijkstra's weakest precondition and factors by proposing a \emph{weakest prespecification} operator |(\\)|, defined by |P;Q `sse` R {-"~"-}<=>{-"~"-} P `sse` (Q\\R)|, where |R| is a specification relating inputs and outputs, and the implementation is decomposed into |P| and |Q|.
The scenario is that |Q| has been designed and |P|, the program yet to be constructed, should be subsumed by a specification |Q\\R|.
Backhouse and van der Woude~\cite{BackhouseWoude:93:Demonic} proposed the concept of \emph{monotype factor} |(`mf`)|, defined by
|range (S . B) `sqse` A {-"~"-}<=>{-"~"-} B `sqse` S `mf` A| where |A| and |B| are restricted to \emph{monotypes} (relations that are subsumed by |id|). In this setting, |(`mf` A)| is the operator which, when applied to a program |S|, computes the weakest liberal precondition for |S| to meet postcondition |A|.
It was demonstrated that monotype factor possesses some properties that are nicer than ordinary factor (called ``spec factor'').
We use factors in a slightly different way: the denominator is the precondition, the numerator is the postcondition, and the factor subsumes the program we aim to compute.
This is due to the way we calculate programs and not a fundamental difference.

This paper intends to be an expository overview rather than a complete theoretical treatment.
A lot remain to be done.
One of the missing ingredient is a satisfactory formulation of converses.
One may define
\begin{equation*}
  |return x `sse` f y| ~\equiv~ |return y `sse` conv f x| \mbox{~~.}
\end{equation*}
However, this definition assume that all monadic programs are formed by either |return| or |mplus|, and therefore does not extend to other effects.
Nor does it give much hint for us to, for example, derive |embed| from |conv choose|.
Hoare~\cite{HoareHe:87:Weakest} mentioned that converses can be defined in terms of factor and negation: |conv Q = {-"\overline{\Varid{Q}}"-} \\ {-"\overline{\Varid{id}}"-}|, where $\overline{\Varid{Q}}$ denotes relational negation of |Q|.
It remains to see whether a notion of converse can be defined for monads in this manner.
\emph{Power transpose} is another missing ingredient.
To model a generate-and-prune style optimisation problem, Bird and de Moor~\cite{BirddeMoor:97:Algebra} would define a relational fold or unfold that maps the input to any candidate solution.
It is then converted to a function that generates the set of all candidates by power transpose, before an optimal solution is chosen from the set.
In our setting, it corresponds to \emph{running} a monadic program inside a monadic program. We have not investigated whether it is a feasible and preferred approach in a monadic calculus.

The monadic calculus we have experimented with so far, however, appears to have good potential. We are looking forward to explore more applications.
