%if False
\begin{code}
{-# LANGUAGE TypeOperators, FlexibleContexts #-}
module Monads where

import Prelude hiding (any, read, readList)
import Control.Monad
import Data.Array

import Common

import QSort
\end{code}
%endif

\section{Monads}
\label{sec:monads}

A monad consists of a type constuctor |m :: * -> *| paired with two operators, usually modelled in Haskell as a type class:
\begin{spec}
class Monad m where
    return  :: a ->  m a
    (>>=)   :: m  a -> (a -> m b) -> m b {-"~~."-}
\end{spec}
They are supposed to satisfy the following \emph{monad laws}:
\begin{align*}
  |m >>= return| &= |m| \mbox{~~,}\\
  |return x >>= f| &= |f x| \mbox{~~,} \\
  |(m >>= f) >>= g| &= |m >>= (\x -> f x >>= g)| \mbox{~~.}
\end{align*}
The operator |return| is usually called $\Varid{return}$ or $\Varid{unit}$.
Since it is used pervasively in this paper, we denote it by a pair of curly brackets (and write its prefix form as |return|) for brevity.
One can either think of it as mimicking the notation for a singleton
set, or C-style syntax for a block of effectful program.
Monadic functions can be combined by Kliseli composition |(>=>)|.
It is also convenient to have an |(<$>)| operator that applies a pure function to a monadic result.
Both operators are defined below:
\begin{spec}
  (>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
  f >=> g {-"~"-}  = {-"~"-} \x -> f x >>= g {-"~~,"-}

  (<$>) :: Monad m => (a -> b) -> m a -> m b
  f <$> m {-"~"-}  = {-"~"-} m >>= \x -> return (f x) {-"~"-}  = {-"~"-} m >>= (return . f) {-"~~."-}
\end{spec}
While |(<$>)| lifts an unary function to monads, |liftM2| lifts a binary operator:
\begin{spec}
liftM2 :: Monad m => (a -> b -> c) -> m a -> m b -> m c
liftM2 oplus m1 m2 = m1 >>= \x1 -> m2 >>= \x2 -> return (x1 `oplus` x2) {-"~~."-}
\end{spec}
Both liftings will be useful in various occasions of this paper.

\paragraph{Non-determinism}
Monads usually come with additional operators corresponding to the effects they provide.
For this paper we are concerned with only two effects --- non-determinism and state.
The latter will be introduced in Section~\ref{sec:iqsort}. Regarding non-determinism, we assume two operators |mzero| and |mplus|, respectively denoting failure and non-deterministic choice:
\begin{spec}
class Monad m => MonadPlus m where
  mzero  :: m a
  mplus  :: m a -> m a -> m a {-"~~."-}
\end{spec}

It might be a good chance to note that this paper use type classes for two purposes: firstly, as a convenient notation to be explicit about the effects a program uses.
For example, programs using non-determinism are labelled with constraint |MonadPlus m|.
Secondly, the notation also implies that it does not matter which actual implementation we use for |m|, as long as the implementation satisfies all properties we demand.
The style of reasoning proposed in this paper is not tied to type classes or Haskell,
and we do not strictly follow the particularities of type classes in the current Haskell standard.%
\footnote{For example, we overlook the particularities that a |Monad| must also be |Applicative|, |MonadPlus| be |Alternative|, and that functional dependency is needed in a number of places in this paper.}

Regarding properties |mplus| and |mzero| ought to satisfy,
it is usually assumed that |mplus| is associative with |mzero| as its identity:
\begin{align*}
  |mzero `mplus` m| & = |m| ~=~ |m `mplus` mzero| \mbox{~~,}
     %\label{eq:mzero-id}
     \\
  |(m1 `mplus` m2) `mplus` m3| &=
     |m1 `mplus` (m2 `mplus` m3)| \mbox{~~.}
     %\label{eq:mplus-assoc}
\end{align*}
We use non-deterministic monads not only as executable programs, but also as specifications.
For the latter purpose, we also assume that |mplus| is idempotent and commutative:
\begin{align*}
  |m `mplus` m| & = |m|  \mbox{~~,}\\
  |m `mplus` n| & = |n `mplus` m| \mbox{~~.}
\end{align*}
This makes the list monad only an approximate implementation.

The laws below concern interaction between non-determinism and |(>>=)|:
\begin{align}
  |mzero >>= f| & = |mzero| \label{eq:nd-left-zero}\mbox{~~,}\\
  |f >> mzero| & = |mzero| \label{eq:nd-right-zero}\mbox{~~,}\\
  |(m1 `mplus` m2) >>= f| &= |(m1 >>= f) `mplus` (m2 >>= f)| \mbox{~~,}
     \label{eq:nd-left-distr}\\
  |m >>= (\x -> f1 `mplus` f2)| &= |(m >>= f1) `mplus` (m >>= f2)| \mbox{~~.}
     \label{eq:nd-right-distr}
\end{align}
Left-zero~\eqref{eq:nd-left-zero} and left-distributivity~\eqref{eq:nd-left-distr} are standard --- the latter says that |mplus| is algebraic.
When mixed with state, right-zero \eqref{eq:nd-right-zero} and right-distributivity \eqref{eq:nd-right-distr} imply that each non-deterministic branch has its own copy of the state.

\paragraph{Refinement} We need a concept of program refinement.
Again we abuse notations from set theory and define:
\begin{spec}
  m1 `sse` m2  {-"~"-}<=>{-"~"-} m1 `mplus` m2 = m2 {-"~~."-}
\end{spec}
Intuitively speaking, the righthand side |m1 `mplus` m2 = m2| says that every result of |m1| is a possible result of |m2|.
When |m1 `sse` m2|, we say that |m2| can be \emph{refined to} |m1|, or that |m2| \emph{subsumes} |m1|.
We denote |sse| lifted to functions by |sqse|:%
\begin{spec}
  f `sqse` g {-"~"-}={-"~"-} (forall x : f x `sse` g x) {-"~~."-}
\end{spec}
That is, |g| can be refined to |f| if, for all |x|, |g x| can be refined to |f x|.%
\footnote{It is an unfortunate coincidence that, in refinement calculus for imperative programs, |p `sqse` q| denotes that |q| is more refined than |p|.
In this paper we let |sqse| be consistent with the direction of |sse|.}

It is not hard showing that the definition of |sse| is equivalent to
\begin{spec}
  m1 `sse` m2  {-"~"-}<=>{-"~"-} (exists n : m1 `mplus` n = m2) {-"~~,"-}
\end{spec}
and that |sse| is reflexive, transitive, and anti-symmetric (that is, |m `sse` n && n `sse` m {-"~"-}<=>{-"~"-} n = m|).
Thus it adopts the principle of proof by \emph{indirect equality}:
\begin{align*}
    |(forall x: x `sse` m <=> x `sse` n)| &~\equiv~ |m = n| \mbox{~~,}\\
    |(forall x: x `sse` m ==> x `sse` n)| &~\equiv~ |m `sse` n| \mbox{~~.}
\end{align*}
That is, to prove that |m = n|, one may instead prove that |x `sse` m| equivals |x `sse` n| for all |x| --- the latter is sometimes easier to prove due to that it involves only |sse|.
When we have only one implication, we get |m `sse` n|.

The following \emph{universal property of |mplus|} is also easy to prove:
\begin{equation}
    |m1 `mplus` m2 `sse` n|~~\equiv~~
       |m1 `sse` n| ~\wedge~ |m2 `sse` n| \mbox{~~.}
       \label{eq:mplus_universal}
\end{equation}
It gives rise to basic but essential properties such as |m1 `sse` m1 `mplus` m2| and |m2 `sse` m1 `mplus` m2}|.

Furthermore, |(>>=)| and pre-composition preserves refinement.
The following proof demonstrates the use of \eqref{eq:nd-left-distr} and \eqref{eq:nd-right-distr}.
\begin{lemma} Refinement is preserved by |(>>=)|. That is,
\begin{enumerate}
\item |m1 `sse` m2 {-"~"-}==>{-"~"-} m1 >>= f `sse` m2 >>= f|.
\item |f1 `sqse` f2 {-"~"-}==>{-"~"-} m >>= f1 `sse` m >>= f2|.
\end{enumerate}
\end{lemma}
\begin{proof}
For 1. we reason:
\begin{spec}
   m2 >>= f
=    {- since |m1 `sse` m2|, assume |m1 `mplus` n = m2|. -}
   (m1 `mplus` n) >>= f
=    {- by \eqref{eq:nd-left-distr}  -}
   (m1 >>= f) `mplus` (n >>= f) {-"~~,"-}
\end{spec}
thus |m1 >>= f `sse` m2 >>= f|.
%
For 2. we reason:
\begin{spec}
   m >>= \x -> f2 x
=    {- since |f1 x `sse` f2 x|, exists |n| -}
   m >>= \x -> f1 x `mplus` n
=    {- by \eqref{eq:nd-right-distr} -}
   (m >>= f1) `mplus` (m >>= \x -> n) {-"~~,"-}
\end{spec}
thus |m >>= f1 `sse` m >>= f2|.
\end{proof}
\begin{lemma}\label{lma:fun-comp-monotonic}
|h `sqse` k ==> (h . f) `sqse` (h . f)|.
\end{lemma}
\begin{proof} We reason:
\begin{spec}
   h `sqse` k  {-"~"-}<=>{-"~"-} (forall x : h x `mplus` k x = k x)
               {-"~"-}==>{-"~"-} (forall x : h (f x) `mplus` k (f x) = k (f x)) {-"~"-}<=>{-"~"-} (h . f) `sqse` (h . f) {-"~~."-}
\end{spec}
\end{proof}

In specifications, we allow non-deterministic monads be defined by first-order logic. For example, the definition below,
\begin{spec}
  return y `sse` atmost x {-"~"-}<=>{-"~"-} y <= x {-"~~,"-}
\end{spec}
%if False
\begin{code}
atmost :: MonadPlus m => A -> m A
atmost = undefined
\end{code}
%endif
says that |y| is a possible result of |atmost x| if |y <= x|.

\paragraph{Factor}
Composition |(>=>)| and refinement order naturally induce the notion of factors, defined by the following two Galois connections:
\begin{align}
  |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}| &  |<=>{-"~"-} g {-"\,"-}`sqse`{-"\,"-} f \\ h {-"~~,"-}| \label{eq:left-factor-galois}\\
  |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}|& |<=>{-"~"-} f {-"\,"-}`sqse`{-"\,"-} h / g {-"~~,"-}| \label{eq:right-factor-galois}
\end{align}
%if False
\begin{code}
(\\) :: Monad m => (a -> m b) -> (a -> m c) -> (b -> m c)
(\\) = undefined
\end{code}
%endif
The types of the two factors are:
\begin{spec}
(\\)  :: Monad m => (a -> m b) -> (a -> m c) -> b -> m c {-"~~,"-}
(/)   :: Monad m => (a -> m c) -> (b -> m c) -> a -> m b {-"~~."-}
\end{spec}
Consider the left factor |(\\)|. Substitute |g| in \eqref{eq:left-factor-galois} for |f \\ h|, we get the cancelation law |f >=> (f\\h) `sqse` h|.
If we read \eqref{eq:left-factor-galois} leftwards, we realize that |f\\h| is the largest function (under |(`sqse`)|) that satisfies |f  >=> (f\\h) `sqse` h|.
The situation with |(/)| is similar.

A factor whose denominator is a pure function can be eliminated, if there exists an inverse for the denominator, as shown in the next lemma. Proof of the lemma demonstrates the use of indirect equality.
\begin{lemma}\label{lma:factor-pure}
Given |f :: a -> b| and |g :: b -> a| such that |f . g = id| and |g . f = id|, we have |(return . g)\\h = h . f| for all |h :: MonadPlus m => b -> m c|. With only |f . g = id|, we have |h . f `sqse` (return . g)\\h|.
\end{lemma}
\begin{proof} By indirect equality. For all |x|,
\begin{spec}
     x `sqse` h . f
==>    {- Lemma~\ref{lma:fun-comp-monotonic}: pre-composition preserves refinement -}
     x . g `sqse` h . f . g
<=>    {- since |f . g = id| -}
     x . g `sqse` h
<=>    {- monad laws -}
     (return . g) >=> x `sqse` h
<=>    {- definition of |(\\)| -}
     x `sqse` (return . g)\\h
<=>  (return . g) >=> x `sqse` h
==>    {- Lemma~\ref{lma:fun-comp-monotonic} -}
     (return . g . f) >=> x `sqse` h . f
<=>    {- since |g . f = id| -}
     x `sqse` h . f {-"~~."-}
\end{spec}
With only |f . g = id|, we may prove the first half: |x `sqse` h . f ==> x `sqse` (return . g)\\h|, and thus get |h . f `sqse` (return . g)\\h|.
\end{proof}

\paragraph{Intersection} Given that |m1 `mplus` m2| is a monad yielding a value that is agreed by either |m1| or |m2|, it makes sense to consider its dual: a monad |m1 `cap` m2| yielding a value approved by \emph{both} |m1| and |m2|.
It can be defined by:
\begin{code}
cap :: (MonadPlus m, Eq a) => m a -> m a -> m a
m1 `cap` m2 =  m1 >>= \x1 -> m2 >>= \x2 ->
               guard (x1 == x2) >> return x1 {-"~~,"-}
\end{code}
where |guard| is a standard function defined by
\begin{spec}
guard :: MonadPlus m => Bool -> m ()
guard b = if b then return (()) else mzero {-"~~."-}
\end{spec}
We also lift |cap| to functions:
|(f1 `sqcap` f2) x = f1 x `cap` f2 x|.
%if False
\begin{code}
(f1 `sqcap` f2) x = f1 x `cap` f2 x
\end{code}
%endif

One may easily prove, using distributivity~\eqref{eq:nd-left-distr}, that intersection distributes into |mplus|.
With that we can also prove that |cap| is monotonic:
\begin{align*}
|(m `mplus` n) `cap` k| &~= |(m `cap` k) `mplus` (n `cap` k)| \mbox{~~,}\\
|m `sse` n| &~\Rightarrow |(m `cap` k) `sse` (n `cap` k)| \mbox{~~.}
\end{align*}
One would expect to have the following properties:
\begin{align*}
  |m1 `cap` m2| &\subseteq |m1| \mbox{~~,} & |m1 `cap` m2| &\subseteq |m2|\mbox{~~,} &
  |m `cap` m| &= |m| \mbox{~~.}
\end{align*}
However, that would require some properties not true in general.
For example, in our definition of |cap|, |m `cap` m| runs |m| twice and might not equal |m|.
One can prove, however, that for monads \emph{whose only effect is non-determinism}, we do have the three properties above.
With them we can prove that |mplus| distributes into |cap|, and the universal property of |cap|:
\begin{align}
 |m `mplus` (n `cap` k)| &~=~ |(m `mplus` n) `cap` (m `mplus` k)| \mbox{~~,} \notag\\
 |m `sse` n && m `sse` k| &~\equiv~ |m `sse` (n `cap` k)|
\mbox{~~.} \label{eq:cap_universal}
\end{align}

\paragraph{Commutativity and |guard|}
We end this section with some notes about commutativity and |guard|.
We say that |m| and |n| commute if
\begin{spec}
m >>= \x -> n >>= \y -> f x y {-"~"-}={-"~"-} n >>= \y -> m >>= \x -> f x y {-"~~."-}
\end{spec}
If |m| and |n| commute for all |m| using effect |X| and |n| using effect |Y|, we say that effects |X| and |Y| commute.

It can be proved that |guard p| commutes with all |m| if non-determinism is the only effect in |m| --- a property we will use a number of times.
Furthermore, having right-zero \eqref{eq:nd-right-zero} and right-distributivity \eqref{eq:nd-right-distr}, in addition to other laws, one can prove that non-determinism commutes with other effects.
In particular, non-determinism commutes with state.

We mention some more properties about |guard| that will be useful. Firstly,
\begin{equation}
|guard (p && q) x| ~=~ |guard p x >> guard q x| \mbox{~~.}
\label{eq:guard-conj-split}
\end{equation}
Secondly, a pair of |guard|s with complementary predicates can be refined to |if|:
\begin{spec}
(guard p x >> m1) `mplus` (guard (not . p) x >> m2) {-"~"-}`spe`{-"~"-} if p then m1 else m2 {-"~~."-}
\end{spec}
