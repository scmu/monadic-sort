{-# LANGUAGE TypeOperators, NPlusKPatterns, FlexibleInstances,
             MultiParamTypeClasses, FunctionalDependencies #-}
module Common where

import Prelude hiding (any, read, readList)
import GHC.Base (Alternative(..))
import Control.Arrow ((***))
import Control.Monad
import Data.Array

type List a = [a]
type Nat = Int
type a :* b = (a,b)
type Triple a b c = (a,b,c)
type T3 a = (a,a,a)

infixl 0 ===
(===) :: a -> a -> a
(===) = undefined

infixl 0 `sse`, `spe`, `sqse`, `sqpe`

sse :: a -> a -> a
sse = undefined

spe :: a -> a -> a
spe = undefined

sqse :: a -> a -> a
sqse = undefined

sqpe :: a -> a -> a
sqpe = undefined

-- MonadArr

class Monad m => MonadArr e m | m -> e where
  read   :: Int -> m e
  write  :: Int -> e -> m ()

  readList :: Int -> Nat -> m (List e)
  readList i 0      = return []
  readList i (k+1)  = liftM2 (:) (read i) (readList (i + 1) k)

  writeList :: Int -> List e -> m ()
  writeList i []      = return (())
  writeList i (x:xs)  = write i x >> writeList (i + 1) xs

  swap :: Int -> Int -> m ()
  swap i j = read i >>= \x -> read j >>= \y -> write j x >> write i y

-- predefined types

type St e = Array Int e

newtype SN e a = SN {unSN :: St e -> [(a, St e)]}

runSN :: SN e a -> Int -> List e -> [(a, List e)]
runSN (SN m) i xs = map (id *** elems) (m (listArray (i, j) xs))
 where j = i + length xs - 1

instance Functor (SN e) where
  fmap f (SN m) = SN (map (f *** id) . m)

instance Applicative (SN e) where
  pure = return
  (<*>) = undefined

instance Monad (SN e) where
  return x = SN (\s -> [(x,s)])
  (SN m) >>= f = SN (concat . map (\(x, s') -> unSN (f x) s') . m)

instance Alternative (SN e) where
  empty = mzero
  (<|>) = mplus

instance MonadPlus (SN e) where
  mzero = SN (\s -> [])
  (SN m1) `mplus` (SN m2) = SN (\s -> m1 s ++ m2 s)

instance MonadArr e (SN e) where
  read i = SN (\s -> [(s!i, s)])
  write i x = SN (\s -> [((), s // [(i,x)])])

  writeList i xs = SN (\s -> [((), s // (zip [i..] xs))])
  swap i j = SN (\s -> [((), s // [(i,s!j), (j,s!i)])])

--

newtype S e a = S {unS :: St e -> (a, St e)}

runS :: S e a -> Int -> List e -> (a, List e)
runS (S m) i xs = (id *** elems) (m (listArray (i, j) xs))
 where j = i + length xs - 1

instance Functor (S e) where
  fmap f (S m) = S ((f *** id) . m)

instance Applicative (S e) where
  pure = return
  (<*>) = undefined

instance Monad (S e) where
  return x = S (\s -> (x,s))
  (S m) >>= f = S ((\(x, s') -> unS (f x) s') . m)

instance MonadArr e (S e) where
  read i = S (\s -> (s!i, s))
  write i x = S (\s -> ((), s // [(i,x)]))

  writeList i xs = S (\s -> ((), s // (zip [i..] xs)))
  swap i j = S (\s -> ((), s // [(i,s!j), (j,s!i)]))



--

class MonadPlus m => Any a m where
  any :: m a {-"~~."-}

type Elm = Int

instance Any Elm [] where
  any = undefined

instance Any a m => Any (List a) m where
  any = return [] `mplus`
        liftM2 (:) any any

--

bold = id
