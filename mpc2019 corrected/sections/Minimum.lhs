%if False
\begin{code}
{-# LANGUAGE MultiParamTypeClasses, TypeSynonymInstances,
             FlexibleInstances, FlexibleContexts #-}
module Minimum where

import Prelude hiding (any)
import Control.Monad

import Common
import Monads
\end{code}
%endif

\section{Minimum}
\label{sec:minimum}

In this section we consider computing a minimum element in a list.
While there is nothing surprising in the resulting program, the specification and calculation are illustrative enough as our first case study.

Let |(<=)| be a total preorder defined on some type |Elm|. That is, |(<=)| is reflexive, transitive, but not necessarily anti-symmetric, and either |x <= y| or |y <= x| for all |x, y :: Elm|.
We assume that there exists a top element |infty :: Elm| such that |x < infty| for all |x :: Elm|.
This value will be used in calculation but not in the final program.
A monadic function
|minimum :: MonadPlus m => List Elm -> m Elm| that computes a minimum element of a given list can be specified using first-order logic:
\begin{equation}
  \{x\} \subseteq \Varid{min}~\Varid{xs} ~\equiv~
     x \in \Varid{xs} \wedge (\forall y \in \Varid{xs} : x \leq y) \mbox{~~.} \label{eq:min-2OL-defn}
\end{equation}
The function is non-deterministic because there can be more than one |x| that satisfies the specification.
The task is to calculate |minlist :: List Elm -> Elm| such that |return . minlist `sqse` minimum|.

\subsection{Specification Using Monads}

To facilitate calculation, we translate the specification~\eqref{eq:min-2OL-defn} to a monadic one.
For that we need to define a number of components.
Firstly, the function |choose| non-deterministically returns one of the elements of a list:
\begin{code}
choose :: MonadPlus m => List a -> m a
choose []      = mzero
choose (x:xs)  = return x `mplus` choose xs {-"~~."-}
\end{code}
Converse to |choose|, we want to define |embed :: a -> m (List a)| such that |embed x| non-deterministically yields a list that contains |x|.
To define |embed| we assume that, for certain first-order types |a| that that include at least |Elm|, there exists a non-determinism monad |any :: m a| that yields an arbitrary value of type |a|. It can be modelled as a type class:
\begin{spec}
class MonadPlus m => Any a m where
  any :: m a {-"~~."-}
\end{spec}
It is not difficult seeing that |Any a m| induces, for example, |Any (List a) m|.
We want |any| to be the most non-deterministic value of its type, that is, |n `sse` any| for all |n| having the right type.

Having |any|, there are a number of ways |embed| can be defined.
For the purpose of this paper we use the following definition:
\begin{spec}
embed :: Any a m => a -> m (List a)
embed x = liftM2 (++[x]++) any any {-"~~,"-}
\end{spec}
%if False
\begin{code}
embed :: Any a m => a -> m (List a)
embed x = liftM2 (\xs ys -> xs++[x]++ys) any any {-"~~,"-}
\end{code}
%endif
where |(++[x]++)| is an abbreviation of |(\ys zs -> ys ++[x] ++ zs)|,
and the two occurrences of |any :: Any a m => m (List a)| non-deterministically compute two lists.
Functions |choose| and |embed| are {\em converses} of each other.
We will not explore the concept in this paper, however (see Section~\ref{sec:conclusions}).

We can now define |minimum| to be:
\begin{code}
minimum ::Any Elm m => List Elm -> m Elm
minimum = choose `sqcap` (embed \\ atmost)  {-"~~."-}
\end{code}
To see how it is a translation of \eqref{eq:min-2OL-defn}, notice that, by universal property of |sqcap|, we have
|minimum `sqse` choose| and |minimum `sqse` embed \\ atmost|.
The first part means that whatever |minimum xs| returns must be a result of |choose xs|, that is, |return x `sse` minimum xs| implies $x \in \Varid{xs}$, the first requirement of \eqref{eq:min-2OL-defn}.

The second part, by definition of |(\\)|, is equivalent to |embed y >>= minimum `sse` atmost y| for all |y|. On the left-hand side we embed |y| to a list, say |xs|, which then becomes the input of |minimum|. Let |x| be a result of |minimum|. That the left-hand side being subsumed by |atmost y| translates to |x <= y|. Assembling the parts, |embed y >>= minimum `sse` atmost y| is equivalent to that |return x `sse` minimum xs| implies |(forall y: y `elem` xs ==> x <=y)|, the second requirement of \eqref{eq:min-2OL-defn}.

\subsection{Derivation}

The task is to construct |minlist :: List Elm -> Elm| such that
\begin{spec}
  return . minlist `sqse` minimum {-"~~."-}
\end{spec}
%Whatever |minlist| returns, it must be a result allowed by |minimum|.
By definition of |minimum| and the universal property of |cap|, it expands to
\begin{align}
|return . minlist| & |`sqse` choose|~~\wedge \label{eq:minlist-spec:01}\\
|return . minlist| & |`sqse` embed \\ atmost| \mbox{~~.} \label{eq:minlist-spec:02}
\end{align}

Consider the requirement~\eqref{eq:minlist-spec:01} or, equivalently,
|return (minlist xs) `sse` choose xs| for all |xs|.
One immediately notices that the inequality cannot hold when |xs := []|, since |choose [] = mzero|.
Therefore, |minlist| {\em cannot be defined for} |[]|.
%
Consider singleton lists: since |choose [x] = return x `mplus` mzero = return x|,
the value for |minlist [x]| is determined already --- |minlist [x] = x|.

Consider the input |x:xs|, while assuming the induction hypothesis that |minlist xs `sse` choose xs|:
\begin{spec}
        choose (x:xs)
=       return x `mplus` choose xs
`spe`   return x `mplus` return (minlist xs)
`spe`     {- guess: |return (x `oplus` y) `sse` return x `mplus` return y|. See below -}
        return (x `oplus` minlist xs) {-"~~."-}
\end{spec}
In the last step, we wish to refine |return x `mplus` return (minlist xs)| to |return (minlist (x:xs))|.
The former is a non-deterministic choice between two pure values, while the latter is one single value.
Let |oplus| be a function that returns one of its two inputs using some criteria yet to be determined, that is, |return (x `oplus` y) `sse` return x `mplus` return y|, one may, without loss of generality, assume that |minlist (x:xs) = x `oplus` minlist xs|.
In summary, our reasoning led us to assume that |minlist| has the following shape:
%if False
\begin{code}
minlist :: List Elm -> Elm
\end{code}
%endif
\begin{code}
minlist [x]     = x
minlist (x:xs)  = x `oplus` minlist xs {-"~~."-}
\end{code}
%if False
\begin{code}
  where x `oplus` y = if x <= y  then x else y
\end{code}
%endif
What remains is to construct |oplus|.

To derive |oplus|, that is, to determine which of the two inputs |oplus| should return, we consider \eqref{eq:minlist-spec:02}.
To proceed, however, the calculation can be made easier if we temporarily assume that |minlist| returns some value for the empty case.
Under the assumption we consider how~\eqref{eq:minlist-spec:02} can be satisfied, and check whether the constraints we find are consistent with |minlist [x] = x|, before dropping the empty case.
Starting with \eqref{eq:minlist-spec:02}:
% The following theorem, which can be proved by routine induction, suggests sufficient conditions with which a monadic function can be refined to a |foldr| or a |foldrn|:
% \begin{theorem}\label{thm:fold-refine}
% For all |f :: a -> b -> b|, |e :: b|, |h :: List a -> M b|, and |g :: a -> b|,
% \begin{align*}
% |return . foldr f e `sqse` h| ~&\Leftarrow~ |return e `sse` h []| ~\wedge~ |f x <$> h xs `sse` h (x:xs)| \mbox{~~,} \\
% |return . foldrn f g `sqse` h| ~&\Leftarrow~ |return (g x) `sse` h [x]| ~\wedge~ |f x <$> h xs `sse` h (x:xs)| \mbox{~~.}
% \end{align*}
% \end{theorem}
% We will use this theorem that allows us to refine a monadic function to a special form of monadic hylomorphism --- an |unfoldrM| followed by a pure |foldr|:
% \begin{theorem}\label{thm:hylo-refine}
% For all |f|, |e|, |g| and |h|,
% \begin{spec}
%    unfoldrM g >=> return . foldr f e `sqse` h {-"~~"-}<==
%     g >=> maybe (return e) (\(x,y) -> f x <$> h y) `sqse` h {-"~~."-}
% \end{spec}
% \end{theorem}
%Now consider \eqref{eq:minlist-spec:02}. We reason:
\begin{spec}
     return . minlist {-"~"-}`sqse` {-"~"-} embed \\ atmost
<=>     {- definition of |(\\)| -}
     embed >=> (return . minlist) {-"~"-}`sqse` {-"~"-} atmost {-"~~."-}
\end{spec}
We reason from the left-hand side, for all |x|:
%format cat3 zs = "{+\!\!\!+}" zs "{+\!\!\!+}"
%if False
\begin{code}
minlist_der1 :: Any Elm m => Elm -> m Elm
minlist_der1 x =
\end{code}
%endif
\begin{code}
   embed x >>= (return . minlist)
 ===     {- definition of |embed| -}
   liftM2 (cat3 [x]) any any >>= (return . minlist)
 ===     {- monad laws -}
   any >>= \ys -> any >>= \zs -> return (minlist (ys ++ [x] ++ zs))
 ===      {- assumption: |oplus| associative, see below -}
   any >>= \ys -> any >>= \zs -> return (minlist ys `oplus` x `oplus` minlist zs)
 ===      {- monad laws -}
   any >>= (return . minlist) >>= \y -> any >>= (return . minlist) >>= \z -> return (y `oplus` x `oplus` z)
 `sse`  {- since |any >>= (return . minlist) `sse` any| -}
   any >>= \y -> any>>= \z -> return (y `oplus` x `oplus` z)
 `sse`  {- wish to be fulfilled -}
   atmost x {-"~~."-}
\end{code}
%if False
\begin{code}
 where cat3 zs xs ys = xs ++ zs ++ ys
       oplus :: Elm -> Elm -> Elm
       oplus = undefined
\end{code}
%endif

In the third step, having to deal with |minlist (ys ++ [x] ++ zs)|, we made another bold guess:
that |oplus| is associative, and therefore |minlist| distributes into |(++)|, that is, |minlist (us ++ ws) = minlist us `oplus` minlist ws| for all |us| and |ws|. This shall be verified after we actually construct |oplus|.

In the penultimate step we claim that |any >>= minlist `sse` any|, where the first and second occurrences of |any| respectively have types |m (List Elm)| and |m Elm| under constraint |Any Elm m|.
This is trivially true since |n `sse` any| for all |n|.
% One of the ways to verify the claim is to see that we have this property about |any|:
% \begin{equation}
% |any| ~\subseteq~ |any >>= \x -> return [x]| \mbox{~~,} \label{eq:any-singleton}
% \end{equation}
% in words, |any|, among many things, could generate a singleton list --- again the two |any| respectively have types |M (List A)| and |M A|.
% Therefore,
% \begin{spec}
%    any >>= minlist
% `sse` {- \eqref{eq:any-singleton} -}
%    any >>= \x -> return [x] >>= minlist
% =     {- since |minlist [x] = x| -}
%    any {-"~~."-}
% \end{spec}
The bottom line is that we shall construct |oplus| such that
\begin{equation*}
    |any >>= \y -> any >>= \z -> return (y `oplus` x `oplus` z)| ~\subseteq~ |atmost x| \mbox{~~,}
\end{equation*}
which expands to |(forall y z: y `oplus` x `oplus` z <= x)|.
One can see that it would not work if |oplus| always return its first or second argument.
Either of the following definitions of |oplus|, however, does satisfy the requirement, if we assume that |minlist [] = infty|:
\begin{spec}
x `oplus` y = if x <= y  then x else y {-"~~,"-}
x `oplus` y = if x < y   then x else y {-"~~,"-}
\end{spec}
Both definitions of |oplus| are associative, and neither contradicts the assumption that |minlist [x] = minlist ([] ++ [x] ++ []) = x|.
Therefore, both make the definition of |minlist| valid.

With this example, we intend to demonstrate that the monadic calculus,
like the relational calculus, allows us to form problem specifications in a concise way, to which laws such as universal properties intersection and factor can be effectively applied.
When necessary, one can effortlessly switch to pointwise style, using case-analysis and induction on the arguments.

% Now consider \eqref{eq:minlist-spec:02}. We reason:
% \begin{spec}
%      return . minlist {-"~"-}`sqse` {-"~"-} embed \\ atmost
% <=>     {- definition of |(\\)| -}
%      embed >=> (return . minlist) {-"~"-}`sqse` {-"~"-} atmost
% <=>     {- definition of |embed| -}
%      (unfoldrM emb . Just) >=> (return . minlist) {-"~"-}`sqse` {-"~"-} atmost
% <=>     {- monad laws, definition of |(\\)| -}
%      unfoldrM emb >=> (return . minlist) {-"~"-}`sqse` {-"~"-} (return . Just) \\ atmost
% <==     {- by Lemma~\ref{lma:factor-pure}, see below -}
%      unfoldrM emb >=> (return . minlist) {-"~"-} `sqse` {-"~"-} atmost . unJust
% <==     {- Theorem~\ref{thm:hylo-refine}-}
%      emb >=> maybe (return e) (\(y,z) -> (y `oplus`) <$> atmost ( unJust z)) {-"~"-}
%         `sqse` {-"~"-} atmost . unJust {-"~~."-}
% \end{spec}
% To use Lemma~\ref{lma:factor-pure} in the third step, we want to construct a function |unJust| such that |unJust . Just = id|.
% We choose:
% \begin{spec}
% unJust (Just x)  = x
% unJust Nothing   = infty {-"~~."-}
% \end{spec}
% We intentionally choose |infty| as the return value for |Nothing|, to facilitate the calculation later.
%
% The aim now is to construct |oplus| such that the last subsumption holds.
% We reason from the right-hand side. For input |Just x|,
% \begin{spec}
%    emb (Just x) >>= maybe (return e) (\(y,z) -> (y `oplus`) <$> atmost (unJust z))
% =     {- definition of |emb| and |skip|, left-distributivity \eqref{eq:nd-left-distr} -}
%    ((x `oplus`) <$> atmost (unJust Nothing)) `mplus`
%    (any >>= \y -> (y `oplus`) <$> atmost (unJust (Just x)))
% =      {- definition of |unJust| -}
%    ((x `oplus`) <$> atmost infty) `mplus` (any >>= \y -> (y `oplus`) <$> atmost x)
% `sse`  {- see below -}
%    atmost x {-"~~."-}
% \end{spec}
% For the penultimate line to be subsumed by |atmost x|, by the universal property of |mplus|, we need both |(x `oplus`) <$> atmost infty `sse` atmost x|
% and |any >>= \y -> (y `oplus`) <$> atmost x `sse` atmost x|. Expanding the definition of |atmost|, we get:
% \begin{align*}
%  |y < infty| &\Rightarrow |x `oplus` y <= x| \mbox{~~,}\\
%  |z <= x| &\Rightarrow |y `oplus` z <= x|\mbox{~~.}
% \end{align*}
% It is not hard to see that choosing either implementations of |oplus| below would satisfy the requirement:
% \begin{spec}
% x `oplus` y = if x <= y  then x else y {-"~~,"-}
% x `oplus` y = if x < y   then x else y {-"~~."-}
% \end{spec}

% The case for input |Nothing| is simple and we omit the details --- for this case we only need to check that choosing |e = infty| meets \eqref{eq:minlist-spec:02}, while allowing |minlist [x] = x|.
% We have therefore constructed (at least two variations of) |minlist|.
