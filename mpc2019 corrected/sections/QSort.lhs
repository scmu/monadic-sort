%if False
\begin{code}
{-# LANGUAGE TypeOperators #-}
module QSort where

import Prelude hiding (any)
import GHC.Base (Alternative(..))
import Control.Monad

import Common
import Monads
\end{code}
%endif

\section{Quicksort}
\label{sec:quicksort}

Bird~\cite{Bird:96:Functional} demonstrated how to derive insertion sort and quicksort~\cite{Hoare:61:Quicksort} from a relational specification.
In this section we show, in finer detail, how quicksort can be derived in our monadic calculus.
We assume a type |Elm| associated with a total preorder |(<=)|.
To sort a list |xs :: List Elm| is to choose, among all permutation of |xs|, those that are sorted:
\begin{code}
slowsort :: MonadPlus m => List Elm -> m (List Elm)
slowsort xs = perm xs >>= filt sorted {-"~~,"-}
\end{code}
where |perm :: MonadPlus m => List a -> m (List a)| non-deterministically computes a permutation of its input, |sorted :: List Elm -> Bool| checks whether a list is sorted, and |filt p x| returns |x| if |p x| holds, and fails otherwise:
\begin{code}
filt :: MonadPlus m => (a -> Bool) -> a -> m a
filt p x = guard (p x) >> return x {-"~~."-}
\end{code}
We now proceed to define |sorted| and |perm|.

\subsection{Sortedness and Permutation}

The predicate |sorted| can be defined by:
\begin{code}
sorted :: List Elm -> Bool
sorted []      = True
sorted (x:xs)  = all (x<=) xs && sorted xs {-"~~."-}
\end{code}
The following property can be proved by a routine induction on |ys|:
\begin{equation}
\begin{split}
&  |sorted (ys ++ [x] ++ zs)| ~\equiv~ \\
&\quad    |sorted ys && sorted zs && all (<=x) ys && all (x<=) zs|  \mbox{~~.}
\end{split}
    \label{eq:sorted-cat3}
\end{equation}

There are many ways |perm| can be defined.
%Like |choose| determining the structure of |minlist|,
Interestingly, the structure of |perm| determines that of the sorting algorithm we derive.
The following definition, for example, would lead us to insertion sort:
%format perm1 = perm
\begin{code}
insert :: MonadPlus m => a -> List a -> m (List a)
insert x []      = return [x]
insert x (y:xs)  = return (x:y:xs) `mplus` ((y:) <$> insert x xs) {-"~~,"-}

perm1 :: MonadPlus m => List a -> m (List a)
perm1 []      = return []
perm1 (x:xs)  = perm1 xs >>= insert x {-"~~."-}
\end{code}
To derive quicksort, we use a different definition of |perm|.
Firstly, the function below non-deterministically splits a list into two, such that |return ((ys,zs)) `sse` split xs| if |xs| can be constructed by interleaving |ys| and |zs|:
\begin{code}
split :: MonadPlus m => List a -> m (List a :* List a)
split []      =  return (([],[]))
split (x:xs)  =  split xs >>= \(ys, zs) -> return ((x:ys, zs)) `mplus` return ((ys, x:zs)) {-"~~."-}
\end{code}
%if False
% \begin{code}
% split :: MonadPlus m => [a] -> m ([a], [a])
% split []      =  return ([],[])
% split (x:xs)  =  split xs >>= \(ys, zs) ->
%                  return (x:ys, zs) `mplus` return (ys, x:zs) {-"~~."-}
% \end{code}
%endif
For example, |split [1,2,3]| has the following outputs:
|([1,2,3],[])|, |([2,3],[1])|, |([1,3],[2])|, |([3],[1,2])|, |([1,2],[3])|, |([2],[1,3])|, |([1],[2,3])|, and |([],[1,2,3])|.
Permutation can then be defined by:
\begin{spec}
perm :: MonadPlus m => List a -> m (List a)
perm []      =  return []
perm (x:xs)  =  split xs >>= \(ys, zs) -> liftM2 (++ [x] ++) (perm ys) (perm zs) {-"~~."-}
\end{spec}
%if False
\begin{code}
perm :: MonadPlus m => List a -> m (List a)
perm []      =  return []
-- perm [x]     =  return [x]
perm (x:xs)  =  split xs >>= \(ys, zs) -> liftM2 (\us vs -> us ++ [x] ++ vs) (perm ys) (perm zs) {-"~~."-}
\end{code}
%endif
When the input |x:xs| has more than one element, we split |xs| into |ys| and |zs|, permute them separately, and insert |x| in the middle. The aim is to construct |qsort| such that
|return . qsort `sqse` slowsort|.

% Simple properties about |guard|:
% \begin{align}
%   |guard p `sqse` guard q| &\Leftarrow |(p <== q)| \mbox{~~,}
%      \label{eq:guard-weaken}\\
%   |guard (p && q)| &\equiv |guard p >> guard q| \mbox{~~.}
% \end{align}

\subsection{Derivation}

The derivation of |qsort| for the base case is easy: by routine reductions we get |qsort [] = []|.
For the inductive case, we may derive |qsort (p:xs)| directly, but instead we derive a property of |slowsort| that can be reused in Section~\ref{sec:iqsort}.
Consider |slowsort (p:xs)|:
%if False
\begin{code}
slowsort_ind_der :: MonadPlus m => Elm -> List Elm -> m (List Elm)
slowsort_ind_der p xs =
\end{code}
%endif
\begin{code}
      slowsort (p:xs)
 ===  perm (p:xs) >>= filt sorted
 ===    {- expanding definitions, monad laws -}
      split xs >>= \(ys, zs) ->
      perm ys >>= \ys' -> perm zs >>= \zs' ->
      filt sorted (ys' ++ [p] ++ zs')
 ===    {- by \eqref{eq:sorted-cat3} -}
      split xs >>= \(ys, zs) ->
      perm ys >>= \ys' -> perm zs >>= \zs' ->
      guard (sorted ys' && sorted zs' && all (<=p) ys' && all (p<=) zs') >>
      return (ys' ++ [p] ++ zs')
 ===    {- |guard| commutes with non-determinism -}
      split xs >>= \(ys, zs) -> guard (all (<=p) ys && all (p<=) zs') >>
      (perm ys >>= filt sorted) >>= \ys' ->
      (perm zs >>= filt sorted) >>= \zs' ->
      return (ys' ++ [p] ++ zs') {-"~~."-}
\end{code}
Provided that we can construct |partition| such that
%if False
\begin{code}
partition_spec :: (Ord a, MonadPlus m) => a -> [a] -> m ([a], [a])
partition_spec p xs =
\end{code}
%endif
\begin{code}
 return (partition p xs) `sse`
   split xs >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs) {-"~~,"-}
\end{code}
we have established that
\begin{equation}
\begin{split}
|slowsort (p:xs)| ~~\supseteq~~ & |return (partition p xs) >>= \(ys, zs) ->|\\[-1mm]
                       & |slowsort ys >>= \ys' -> slowsort zs >>= \zs' ->|\\[-1mm]
                       & |return (ys' ++ [p] ++ zs') {-"~~."-}|
\end{split}
\label{eq:slowsort-rec}
\end{equation}

A derivation works by constructing |qsort| and, at the same time, building a proof that |qsort| satisfies the specification.
In this case, the proof works by induction on the size of its input.
Therefore, we assume that the specification |return (qsort ws) `sse` perm ws >>= filt sorted| holds for |ws| whose length is shorter than |p:xs|. In particular, it holds for |ys| and |zs|. We therefore have:
\begin{spec}
      slowsort (p:xs)
 `spe` {- \eqref{eq:slowsort-rec} and inductive assumption -}
      return (partition p xs) >>
      return (qsort ys) >>= \ys' -> return (qsort zs) >>= \zs' ->
      return (ys' ++ [p] ++ zs')
 ===   {- monad laws -}
      return (let (ys,zs) = partition p xs in qsort ys ++ [p] ++ qsort zs) {-"~~."-}
\end{spec}
In summary, we have derived
\begin{code}
qsort []      =  []
qsort (p:xs)  =  let (ys,zs) = partition p xs
                 in qsort ys ++ [p] ++ qsort zs {-"~~."-}
\end{code}

The remaining task is to construct |partition p xs|. The case for |xs := []| is again immediate.
Consider |x:xs|, assuming that the specification holds for |xs|:
%if False
\begin{code}
partition_ind_der ::
  (MonadPlus m, Ord a) => a -> a -> [a] -> m ([a], [a])
partition_ind_der p x xs =
\end{code}
%endif
\begin{code}
   split (x:xs) >>= filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
 ===       {- definition of |split| -}
   (split xs >>= \(ys, zs) -> return (x:ys, zs) `mplus` return (ys, x:zs)) >>=
      filt (\(ys, zs) -> all (<=p) ys && all (p<=) zs)
 ===       {- monad laws, left-distributivity, definition of |filt| -}
   split xs >>= \(ys, zs) ->
     (guard (all (<=p) (x:ys) && all (p<=) zs) >> return (x:ys, zs)) `mplus`
     (guard (all (<=p) ys && all (p<=) (x:zs)) >> return (ys, x:zs))
 ===       {- definition of |all|, \eqref{eq:guard-conj-split} -}
   split xs >>= \(ys, zs) ->
    (guard (all (<=p) ys && all (p<=) zs) >> guard (x<=p) >> return (x:ys, zs)) `mplus`
    (guard (all (<=p) ys && all (p<=) zs) >> guard (p<=x) >> return (ys, x:zs))
 ===       {- commutativity of |guard|, \eqref{eq:guard-conj-split} -}
   split xs >>= \(ys, zs) -> guard (all (<=p) ys && all (p<=) zs) >>
    ((guard (x<=p) >> return (x:ys, zs)) `mplus` (guard (p<=x) >> return (ys, x:zs)))
 `spe`   {- inductive assumption -}
   return (partition p xs) >>= \(ys,zs) ->
   ((guard (x<=p) >> return (x:ys, zs)) `mplus` (guard (p<=x) >> return (ys, x:zs)))
 `spe`   {- refining |guard| to branching -}
   return (  let (ys,zs) = partition p xs
             in if x <= p then (x:ys, zs) else (ys, x:zs)) {-"~~."-}
\end{code}
The last step is where resolved the tie: when |x| and |p| equal, the specification allows us to place |x| in either partition, and we chose the left partition for no particular reason.
We therefore conclude:
\begin{code}
partition p []      =  ([],[])
partition p (x:xs)  =  let (ys,zs) = partition p xs
                       in if x <= p then (x:ys, zs) else (ys, x:zs) {-"~~."-}
\end{code}
