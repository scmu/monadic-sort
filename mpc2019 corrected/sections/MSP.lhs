%if False
\begin{code}
{-# LANGUAGE MultiParamTypeClasses, TypeSynonymInstances,
             FlexibleInstances, FlexibleContexts #-}
module MSP where

import Prelude hiding (any)
import Control.Monad

import Common
import Monads
\end{code}
%endif

\section{Maximum-Sum Prefix}
\label{sec:msp}

For the second example we consider a simple optimisation problem: given a list, compute a prefix whose sum is maximum. It is a key component for solving the classical maximum-sum segment problem.
Given a preorder |unlhd| on some type |A|, |maximum :: MonadPlus m => List A -> m A|, to be defined in Section~\ref{sec:maximum}, is a monadic function such that |maximum xs| non-deterministically yields an element of |xs| that is maximum in |xs| with respect to |unlhd|.
For this problem, we let |A = List Int| and let |unlhd| be defined by:
\begin{spec}
  xs `unlhd` ys {-"~"-}<=>{-"~"-} sum xs <= sum ys {-"~~."-}
\end{spec}
The maximum-sum prefix problem can be specified by:
\begin{spec}
 return . msp `sqse` maximum . inits {-"~~,"-}
\end{spec}
where |inits| is the standard Haskell function computing all prefixes of a list:
\begin{spec}
inits :: List a -> List (List a)
inits []      = [[]]
inits (x:xs)  = [] : map (x:) (inits xs) {-"~~."-}
\end{spec}

\subsection{Intersection, Factor, and Converses}
\label{sec:maximum}

This section defines a missing ingredient, |maximum|, after we introduce monadic counterparts of three important relational operators: intersection, factor, and converses.


\subsubsection{Intersection} While |m1 `mplus` m2| is a monad yielding a value that is agreed by either |m1| or |m2|, a computation |m1 `cap` m2| yields a value approved by \emph{both} |m1| and |m2|.
As its definition, we let |m1 `cap` m2| be a monadic value satisfying that for all |n|:
\begin{spec}
n `sse` m1 `cap` m2 {-"~"-}<=>{-"~"-} n `sse` m1 && n `sse` m2 {-"~~,"-}
\end{spec}
and also lift |cap| to functions: |(f1 `sqcap` f2) x = f1 x `cap` f2 x|.
%if False
\begin{code}
(f1 `sqcap` f2) x = f1 x `cap` f2 x
\end{code}
%endif

It can be defined by:
\begin{code}
cap :: (MonadPlus m, Eq a) => m a -> m a -> m a
m1 `cap` m2 =  m1 >>= \x1 -> m2 >>= \x2 ->
               guard (x1 == x2) >> return x1 {-"~~,"-}
\end{code}
where |guard| is a standard function defined by
\begin{spec}
guard :: MonadPlus m => Bool -> m ()
guard b = if b then return (()) else mzero {-"~~."-}
\end{spec}


One may easily prove, using distributivity~\eqref{eq:nd-left-distr}, that intersection distributes into |mplus|.
With that we can also prove that |cap| is monotonic:
\begin{align*}
|(m `mplus` n) `cap` k| &~= |(m `cap` k) `mplus` (n `cap` k)| \mbox{~~,}\\
|m `sse` n| &~\Rightarrow |(m `cap` k) `sse` (n `cap` k)| \mbox{~~.}
\end{align*}
One would expect to have the following properties:
\begin{align*}
  |m1 `cap` m2| &\subseteq |m1| \mbox{~~,} & |m1 `cap` m2| &\subseteq |m2|\mbox{~~,} &
  |m `cap` m| &= |m| \mbox{~~.}
\end{align*}
However, that would require some properties not true in general.
For example, in our definition of |cap|, |m `cap` m| runs |m| twice and might not equal |m|.
One can prove that for monads \emph{whose only effect is non-determinism}, we do have the three properties above.
With them we can prove that |mplus| distributes into |cap|, and the universal property of |cap|:
\begin{align}
 |m `mplus` (n `cap` k)| &~=~ |(m `mplus` n) `cap` (m `mplus` k)| \mbox{~~,} \notag\\
 |m `sse` n && m `sse` k| &~\equiv~ |m `sse` (n `cap` k)|
\mbox{~~.} \label{eq:cap_universal}
\end{align}
These properties will only be needed in the first half of this paper when non-determinism is the only effect, not after state is involved.


\subsubsection{Factor}
Composition |(>=>)| and refinement order naturally induce the notion of (left) factor, defined by the following Galois connection:
\begin{align}
  |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}| &  |<=>{-"~"-} g {-"\,"-}`sqse`{-"\,"-} f \\ h {-"~~."-}| \label{eq:left-factor-galois}
\end{align}
% \begin{align}
%   |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}| &  |<=>{-"~"-} g {-"\,"-}`sqse`{-"\,"-} f \\ h {-"~~,"-}| \label{eq:left-factor-galois}\\
%   |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}|& |<=>{-"~"-} f {-"\,"-}`sqse`{-"\,"-} h / g {-"~~,"-}| \label{eq:right-factor-galois}
% \end{align}
%if False
\begin{code}
(\\) :: Monad m => (a -> m b) -> (a -> m c) -> (b -> m c)
(\\) = undefined
\end{code}
%endif
The type of |(\\)| is |(a -> m b) -> (a -> m c) -> b -> m c|, for |Monad m|.
% \begin{spec}
% (\\)  :: Monad m => (a -> m b) -> (a -> m c) -> b -> m c {-"~~,"-}
% (/)   :: Monad m => (a -> m c) -> (b -> m c) -> a -> m b {-"~~."-}
% \end{spec}
Factors are closely related to pre- and post-conditions --- a more through review will be given in Section~\ref{sec:conclusions}.
The left factor |(\\)| will be used a lot in specifications in this paper. Substitute |g| in \eqref{eq:left-factor-galois} for |f \\ h|, we get the cancelation law |f >=> (f\\h) `sqse` h|.
If we read \eqref{eq:left-factor-galois} leftwards,
\begin{align*}
|f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-}| &  |<=={-"~"-} g {-"\,"-}`sqse`{-"\,"-} f \\ h {-"~~."-}|
\end{align*}
we see that |f \\ h| is the largest function (with respect to |(`sqse`)|) that satisfies |f  >=> (f \\ h) `sqse` h|.
Let |h :: a -> m c| be a monadic specification, and |f :: a -> m b| performs the computation half way.
Then |f \\ h| is the largest (meaning least constrained) monadic program that meets the result specified by |h| when ran after the postcondition set up by |f|. In this paper we are often given |h| and |f|, and attempt to construct a refinement of |f \\ h|.

Dually, there is a right factor |(/)| defined by |f >=> g {-"\,"-}`sqse`{-"\,"-} h {-"~"-} <=>{-"~"-} f {-"\,"-}`sqse`{-"\,"-} h / g|, which is also important but will not be used in this paper.

% A factor whose denominator is a pure function can be eliminated, if there exists an inverse for the denominator, as shown in the next lemma. Proof of the lemma demonstrates the use of indirect equality.
% \begin{lemma}\label{lma:factor-pure}
% Given |f :: a -> b| and |g :: b -> a| such that |f . g = id| and |g . f = id|, we have |(return . g)\\h = h . f| for all |h :: MonadPlus m => b -> m c|. With only |f . g = id|, we have |h . f `sqse` (return . g)\\h|.
% \end{lemma}
% \begin{proof} By indirect equality. For all |x|,
% \begin{spec}
%      x `sqse` h . f
% ==>    {- Lemma~\ref{lma:fun-comp-monotonic}: pre-composition preserves refinement -}
%      x . g `sqse` h . f . g
% <=>    {- since |f . g = id| -}
%      x . g `sqse` h
% <=>    {- monad laws -}
%      (return . g) >=> x `sqse` h
% <=>    {- definition of |(\\)| -}
%      x `sqse` (return . g)\\h
% <=>  (return . g) >=> x `sqse` h
% ==>    {- Lemma~\ref{lma:fun-comp-monotonic} -}
%      (return . g . f) >=> x `sqse` h . f
% <=>    {- since |g . f = id| -}
%      x `sqse` h . f {-"~~."-}
% \end{spec}
% With only |f . g = id|, we may prove the first half: |x `sqse` h . f ==> x `sqse` (return . g)\\h|, and thus get |h . f `sqse` (return . g)\\h|.
% \end{proof}
