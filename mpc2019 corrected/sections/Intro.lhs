\section{Introduction}

Program derivation is the technique for formally constructing a program from a problem specification.
% in a stepwise manner such that every step is rigorously justified.
Both functional and imperative programming paradigms have their traditions of program derivation.
In functional programming, the most well-known example is probably the \emph{maximum segment sum} problem: given a list of numbers, find the largest possible sum of a consecutive segment.
Formally, the problem can be specified by a function |mss :: List Int -> Int|:
\begin{spec}
  mss = maximum . map sum . segments {-"~~."-}
\end{spec}
It is not hard to see that the specification captures the textual description above:
|segments :: List Int -> List (List Int)| generates all consecutive segments, the sum of each segment is computed by |map sum|, from which the maximum is chosen.%
\footnote{One possible definition is |segments = concat . map inits . tails|. We will not go into details of this problem.
Interested readers may find more information in
Gibbons~\cite{Gibbons:94:Introduction} or \cite{Gibbons:97:Calculating}.}
If executed, |mss| defined above is a very slow algorithm. However, one can discover a faster version by equational reasoning:
\begin{spec}
   maximum . map sum . segments
=  ....
=  snd . foldr step (0,0) {-"~~."-}
\end{spec}
The last line of the derivation, where |step a (b, m) = (c, max m c) where c = max 0 (a + b)|, is the well-known linear-time algorithm.
It is derived from the specification through a chain of equalities, each of them justified by axioms or proven properties --- without the derivation it is not easy to see why the algorithm is correct.
Ideally, the shape of the final algorithm (that it uses a |foldr|, for example) and definitions of components such as |step| are discovered during the derivation.
The final program and its proof of correctness are developed together.

\paragraph{Need for non-determinism}
That |snd . foldr step (0,0)| equals |mss| indicates that the final program has to return exactly what the specification returns.
Consider the situation when we want to compute not just a sum, but the actual segment.
There could be more than one segments having the same minimum sum.
It is the specification that decides which one to return (the choice probably made in |maximum|), and the implementation has no choice other than to obey.
Such inflexibility may turn out to be a severe restriction.
%It is often the case that, for certain inputs to a problem, several answers are equally preferred.
Take sorting for example. The array to be sorted might contain items with identical keys.
It would be very inflexible having to decide in the specification how to resolve the tie: we might lose some efficiency if the specification demands that the sorting be stable, for example.
It might not even be possible: it is hard to describe, in the specification, how quicksort arranges elements with identical keys, before actually deriving quicksort.

Such problems are better modelled as non-deterministic mappings from the input to all valid outputs.
A specification of sorting, for example, maps the input list to any sorted output.
Rather than demanding that the derived program being equal to the specification, we stepwise refine the specification to a more deterministic one.
In each step of the derivation, we are given flexibility to choose, according to various concerns of the algorithm being designed, which output(s) to return.
In fact, this is standard in imperative program derivation:
Dijkstra~\cite{Dijkstra:76:Discipline}
%, when designing Guarded Command Language,
argued that a calculus for program derivation should take non-determinism as default and determinism as a special case.

\paragraph{Non-determinism in functional program derivation}
To cope with non-determinism in program derivation in the functional paradigm, in the 90's there was a trend generalising from functions to relations.
%non-deterministic mappings between inputs and outputs.
Backhouse et al.~\cite{BackhousedeBruin:91:Relational} generalised catamorphism to relations,
and a theory of datatypes was developed basing on generalisation
from functors to relators~\cite{Backhouse:91:Polynomial}.
Bird and de Moor~\cite{BirddeMoor:97:Algebra} presented various examples of program derivation, in particular those for optimisation problems, in the category of sets and relations.
While their calculus was in practice always instantiated to relations, a large proportion of the theory was presented in a more general setting, for distributive, division, power \emph{allegories} --- that is, categories extended with operations including intersection, union, factors, and a notion of ordering used to model refinement.
%
Although the calculus was, for advocates including the authors of this paper, concise and elegant, for those who were not following this line of development, the barrier between the relational calculus and the rest of the functional programming community was hard to overcome.
It was complained that the concept of relations is too hard, the
notations too bizarre, and reasoning with inequality (refinement) too complex.

One source of difficulties is that notations of relational calculus are usually \emph{point-free} --- that is, about composing relations instead of applying relations to arguments.
% There has been attempts (e.g \cite{deMoorGibbons:00:Pointwise}) designing a pointwise notation, which functional programmers are more familiar with, for relations.
% Such designs tend to get confusing when a non-deterministic value is passed to a function. %Comparison and discussions will be given in Section~\ref{sec:conclusions}.
There has been attempts designing a pointwise notation, which functional programmers are more familiar with, for relations,
Proposals along this line tend to exhibit confusion when a non-deterministic value is passed to a function --- it appears that $\beta$-reduction and $\eta$-conversion do not always hold.
De Moor and Gibbons~\cite{deMoorGibbons:00:Pointwise}, for example, proposed a language for non-deterministic computation that has variables, and suggested a categorial semantics.
One of their examples is that |(\x -> x - x) (0 `mplus` 1)|, where |mplus| denotes non-deterministic choice, always yields |0|, while |(0 `mplus` 1) - (0 `mplus` 1)| could be |0|, |1|, or |-1|.
% A monadic calculus avoids these anomaly by distinguishing pure and non-deterministic values by types. Letting |m = return 0 `mplus` return 1|, the two examples are respectively written |m >>= \x -> return (x - x)| and |m >>= \x1 -> m >>= \x2 -> return (x1 - x2)| in our calculus --- clearly different programs.
More recently, Bird and Rabe \cite{BirdRabe:19:How} also proposed a language for non-deterministic functions and gave a denotational semantics.
They used a predicate to distinguish pure and non-deterministic values.
It is stated that $\beta$-reduction holds only for pure arguments, and $\eta$-conversion only for pure functions.
% When a non-deterministic value is passed to a function (which may also have been computed non-deterministically), rules similar to monadic bind is applied.
% Thus we believe that our approaches share some essence in common ---
% there is a trade off whether making the bind explicit, and we chose different ways.
For a tricky example, Bird and Rabe mentioned that
|(\x -> x `mplus` 3)| and |id `mplus` const 3|, while being extensionally equal, can be distinguished by certain context.
% These two functions would have different types in our calculus.

\paragraph{Monads for program derivation}
Preceding the development of relations for program derivation, another way to model non-determinism has gained popularity.
Monads~\cite{Moggi:89:Computational} were introduced into the world of functional programming as a way to rigorously talk about side effects including IO, state, exception, and non-determinism.
Although they are also considered one of the main obstacles in learning functional programming (in particular Haskell), monads have gained relatively wide acceptance.

In this paper we propose a calculus of program derivation based on monads --- essentially moving to a Kleisli category.
In this calculus:
\begin{itemize}
\item We propose that problem specifications be given as non-deterministic monads, to be refined to deterministic functional programs through calculation.
\item One of the benefits is that functional programmers may deploy techniques they are familiar with when reasoning about and deriving programs. These include both point-free and pointwise reasoning, and induction on structures or sizes of data types.
\item While we do not claim to have established a precise parallel to allegories, many of the key ingredients of the relational approach, including union, intersection, refinement (inclusion ordering), and factor, etc., do have their counterparts in monads that can be used in program calculation.
\item An additional benefit of using monads is that we may talk about effects other than non-determinism. We demonstrate how to, from a specification of quicksort on lists, construct the imperative quicksort for arrays.
It is known in the community that the notion of factors is closely related to pre and postconditions.
Indeed, in our case studies, imperative programs can be modelled using factors, and their calculation mimics construction of imperative programs by reasoning backwards from postconditions.
\end{itemize}

This paper aims to be an example-driven tutorial of monadic program derivation, rather than a complete theoretical treatment.
The key properties, however, have been verified by the proof assistant Agda.
After introducing essential operators for non-deterministic monads in Section~\ref{sec:monads},
we present our first example --- selecting a minimum element of a list, in Section~\ref{sec:minimum}.
In Section~\ref{sec:quicksort} we present a derivation of quicksort, from a general specification of sorting --- compute all permutations of a list before returning a sorted one.
We then introduce operations on mutable arrays as another effect, and show in Section~\ref{sec:iqsort} that the specification for sorting can be refined to the well-known quicksort for arrays.
In Section~\ref{sec:conclusions} we discuss related and future work.
